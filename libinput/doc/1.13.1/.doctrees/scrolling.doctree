��am      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �block_quote���)��}�(hhh]��
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�h	�parent�hhh�source�N�line�Nubh �target���)��}�(h�.. _scrolling:�h]�h}�(h]�h]�h]�h]�h]��refid��	scrolling�uhhhKhhhhh�5/home/whot/code/libinput/build/doc/user/scrolling.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�	Scrolling�h]�h �Text����	Scrolling�����}�(hh6hh4hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hh/hhhh,hKubh �	paragraph���)��}�(h��libinput supports three different types of scrolling methods:
:ref:`twofinger_scrolling`, :ref:`edge_scrolling` and
:ref:`button_scrolling`. Some devices support multiple methods, though only
one can be enabled at a time. As a general overview:�h]�(h9�>libinput supports three different types of scrolling methods:
�����}�(h�>libinput supports three different types of scrolling methods:
�hhFhhhNhNub�sphinx.addnodes��pending_xref���)��}�(h�:ref:`twofinger_scrolling`�h]�h �inline���)��}�(hhTh]�h9�twofinger_scrolling�����}�(hhhhXubah}�(h]�h]�(�xref��std��std-ref�eh]�h]�h]�uhhVhhRubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�hc�refexplicit���	reftarget��twofinger_scrolling��refdoc��	scrolling��refwarn��uhhPhh,hKhhFubh9�, �����}�(h�, �hhFhhhNhNubhQ)��}�(h�:ref:`edge_scrolling`�h]�hW)��}�(hh~h]�h9�edge_scrolling�����}�(hhhh�ubah}�(h]�h]�(hb�std��std-ref�eh]�h]�h]�uhhVhh|ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�h��refexplicit��hr�edge_scrolling�hthuhv�uhhPhh,hKhhFubh9� and
�����}�(h� and
�hhFhhhNhNubhQ)��}�(h�:ref:`button_scrolling`�h]�hW)��}�(hh�h]�h9�button_scrolling�����}�(hhhh�ubah}�(h]�h]�(hb�std��std-ref�eh]�h]�h]�uhhVhh�ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�h��refexplicit��hr�button_scrolling�hthuhv�uhhPhh,hKhhFubh9�i. Some devices support multiple methods, though only
one can be enabled at a time. As a general overview:�����}�(h�i. Some devices support multiple methods, though only
one can be enabled at a time. As a general overview:�hhFhhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKhh/hhubh �bullet_list���)��}�(hhh]�(h �	list_item���)��}�(h�_touchpad devices with physical buttons below the touchpad support edge and
two-finger scrolling�h]�hE)��}�(h�_touchpad devices with physical buttons below the touchpad support edge and
two-finger scrolling�h]�h9�_touchpad devices with physical buttons below the touchpad support edge and
two-finger scrolling�����}�(hh�hh�ubah}�(h]�h]�h]�h]�h]�uhhDhh,hKhh�ubah}�(h]�h]�h]�h]�h]�uhh�hh�hhhh,hNubh�)��}�(h�utouchpad devices without physical buttons (:ref:`ClickPads <clickpad_softbuttons>`)
support two-finger scrolling only�h]�hE)��}�(h�utouchpad devices without physical buttons (:ref:`ClickPads <clickpad_softbuttons>`)
support two-finger scrolling only�h]�(h9�+touchpad devices without physical buttons (�����}�(h�+touchpad devices without physical buttons (�hh�ubhQ)��}�(h�':ref:`ClickPads <clickpad_softbuttons>`�h]�hW)��}�(hh�h]�h9�	ClickPads�����}�(hhhh�ubah}�(h]�h]�(hb�std��std-ref�eh]�h]�h]�uhhVhh�ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�j  �refexplicit��hr�clickpad_softbuttons�hthuhv�uhhPhh,hKhh�ubh9�#)
support two-finger scrolling only�����}�(h�#)
support two-finger scrolling only�hh�ubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKhh�ubah}�(h]�h]�h]�h]�h]�uhh�hh�hhhh,hNubh�)��}�(h�6pointing sticks provide on-button scrolling by default�h]�hE)��}�(hj%  h]�h9�6pointing sticks provide on-button scrolling by default�����}�(hj%  hj'  ubah}�(h]�h]�h]�h]�h]�uhhDhh,hKhj#  ubah}�(h]�h]�h]�h]�h]�uhh�hh�hhhh,hNubh�)��}�(h�]mice and other pointing devices support on-button scrolling but it is not
enabled by default
�h]�hE)��}�(h�\mice and other pointing devices support on-button scrolling but it is not
enabled by default�h]�h9�\mice and other pointing devices support on-button scrolling but it is not
enabled by default�����}�(hj@  hj>  ubah}�(h]�h]�h]�h]�h]�uhhDhh,hKhj:  ubah}�(h]�h]�h]�h]�h]�uhh�hh�hhhh,hNubeh}�(h]�h]�h]�h]�h]��bullet��-�uhh�hh,hKhh/hhubhE)��}�(hX  A device may differ from the above based on its capabilities. See
**libinput_device_config_scroll_set_method()** for documentation on how to
switch methods and **libinput_device_config_scroll_get_methods()** for
documentation on how to query a device for available scroll methods.�h]�(h9�BA device may differ from the above based on its capabilities. See
�����}�(h�BA device may differ from the above based on its capabilities. See
�hjZ  hhhNhNubh �strong���)��}�(h�.**libinput_device_config_scroll_set_method()**�h]�h9�*libinput_device_config_scroll_set_method()�����}�(hhhje  ubah}�(h]�h]�h]�h]�h]�uhjc  hjZ  ubh9�0 for documentation on how to
switch methods and �����}�(h�0 for documentation on how to
switch methods and �hjZ  hhhNhNubjd  )��}�(h�/**libinput_device_config_scroll_get_methods()**�h]�h9�+libinput_device_config_scroll_get_methods()�����}�(hhhjx  ubah}�(h]�h]�h]�h]�h]�uhjc  hjZ  ubh9�I for
documentation on how to query a device for available scroll methods.�����}�(h�I for
documentation on how to query a device for available scroll methods.�hjZ  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKhh/hhubh)��}�(h�.. _horizontal_scrolling:�h]�h}�(h]�h]�h]�h]�h]�h*�horizontal-scrolling�uhhhKhh/hhhh,ubh.)��}�(hhh]�(h3)��}�(h�Horizontal scrolling�h]�h9�Horizontal scrolling�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj�  hhhh,hKubhE)��}�(hX�  Scroll movements provide vertical and horizontal directions, each
scroll event contains both directions where applicable, see
**libinput_event_pointer_get_axis_value()**. libinput does not provide separate
toggles to enable or disable horizontal scrolling. Instead, horizontal
scrolling is always enabled. This is intentional, libinput does not have
enough context to know when horizontal scrolling is appropriate for a given
widget. The task of filtering horizontal movements is up to the caller.�h]�(h9�~Scroll movements provide vertical and horizontal directions, each
scroll event contains both directions where applicable, see
�����}�(h�~Scroll movements provide vertical and horizontal directions, each
scroll event contains both directions where applicable, see
�hj�  hhhNhNubjd  )��}�(h�+**libinput_event_pointer_get_axis_value()**�h]�h9�'libinput_event_pointer_get_axis_value()�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhjc  hj�  ubh9XH  . libinput does not provide separate
toggles to enable or disable horizontal scrolling. Instead, horizontal
scrolling is always enabled. This is intentional, libinput does not have
enough context to know when horizontal scrolling is appropriate for a given
widget. The task of filtering horizontal movements is up to the caller.�����}�(hXH  . libinput does not provide separate
toggles to enable or disable horizontal scrolling. Instead, horizontal
scrolling is always enabled. This is intentional, libinput does not have
enough context to know when horizontal scrolling is appropriate for a given
widget. The task of filtering horizontal movements is up to the caller.�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKhj�  hhubh)��}�(h�.. _twofinger_scrolling:�h]�h}�(h]�h]�h]�h]�h]�h*�twofinger-scrolling�uhhhK,hj�  hhhh,ubeh}�(h]�(j�  �id2�eh]�h]�(�horizontal scrolling��horizontal_scrolling�eh]�h]�uhh-hh/hhhh,hK�expect_referenced_by_name�}�j�  j�  s�expect_referenced_by_id�}�j�  j�  subh.)��}�(hhh]�(h3)��}�(h�Two-finger scrolling�h]�h9�Two-finger scrolling�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj�  hhhh,hK+ubhE)��}�(h��The default on two-finger capable touchpads (almost all modern touchpads are
capable of detecting two fingers). Scrolling is triggered by two fingers
being placed on the surface of the touchpad, then moving those fingers
vertically or horizontally.�h]�h9��The default on two-finger capable touchpads (almost all modern touchpads are
capable of detecting two fingers). Scrolling is triggered by two fingers
being placed on the surface of the touchpad, then moving those fingers
vertically or horizontally.�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK-hj�  hhubh �figure���)��}�(hhh]�(h �image���)��}�(h�i.. figure:: twofinger-scrolling.svg
    :align: center

    Vertical and horizontal two-finger scrolling
�h]�h}�(h]�h]�h]�h]�h]��uri��twofinger-scrolling.svg��
candidates�}��*�j  suhj  hj  hh,hK5ubh �caption���)��}�(h�,Vertical and horizontal two-finger scrolling�h]�h9�,Vertical and horizontal two-finger scrolling�����}�(hj   hj  ubah}�(h]�h]�h]�h]�h]�uhj  hh,hK5hj  ubeh}�(h]��id5�ah]�h]�h]�h]��align��center�uhj  hK5hj�  hhhh,ubhE)��}�(hX�  For scrolling to trigger, a built-in distance threshold has to be met but once
engaged any movement will scroll. In other words, to start scrolling a
sufficiently large movement is required, once scrolling tiny amounts of
movements will translate into tiny scroll movements.
Scrolling in both directions at once is possible by meeting the required
distance thresholds to enable each direction separately.�h]�h9X�  For scrolling to trigger, a built-in distance threshold has to be met but once
engaged any movement will scroll. In other words, to start scrolling a
sufficiently large movement is required, once scrolling tiny amounts of
movements will translate into tiny scroll movements.
Scrolling in both directions at once is possible by meeting the required
distance thresholds to enable each direction separately.�����}�(hj7  hj5  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK7hj�  hhubhE)��}�(h��When a scroll gesture remains close to perfectly straight, it will be held to
exact 90-degree angles; but if the gesture moves diagonally, it is free to
scroll in any direction.�h]�h9��When a scroll gesture remains close to perfectly straight, it will be held to
exact 90-degree angles; but if the gesture moves diagonally, it is free to
scroll in any direction.�����}�(hjE  hjC  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK>hj�  hhubhE)��}�(hX�  Two-finger scrolling requires the touchpad to track both touch points with
reasonable precision. Unfortunately, some so-called "semi-mt" touchpads can
only track the bounding box of the two fingers rather than the actual
position of each finger. In addition, that bounding box usually suffers from
a low resolution, causing jumpy movement during two-finger scrolling.
libinput does not provide two-finger scrolling on those touchpads.�h]�h9X�  Two-finger scrolling requires the touchpad to track both touch points with
reasonable precision. Unfortunately, some so-called “semi-mt” touchpads can
only track the bounding box of the two fingers rather than the actual
position of each finger. In addition, that bounding box usually suffers from
a low resolution, causing jumpy movement during two-finger scrolling.
libinput does not provide two-finger scrolling on those touchpads.�����}�(hjS  hjQ  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKBhj�  hhubh)��}�(h�.. _edge_scrolling:�h]�h}�(h]�h]�h]�h]�h]�h*�edge-scrolling�uhhhKNhj�  hhhh,ubeh}�(h]�(�two-finger-scrolling�j�  eh]�h]�(�two-finger scrolling��twofinger_scrolling�eh]�h]�uhh-hh/hhhh,hK+j�  }�jp  j�  sj�  }�j�  j�  subh.)��}�(hhh]�(h3)��}�(h�Edge scrolling�h]�h9�Edge scrolling�����}�(hjz  hjx  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hju  hhhh,hKMubhE)��}�(h��On some touchpads, edge scrolling is available, triggered by moving a single
finger along the right edge (vertical scroll) or bottom edge (horizontal
scroll).�h]�h9��On some touchpads, edge scrolling is available, triggered by moving a single
finger along the right edge (vertical scroll) or bottom edge (horizontal
scroll).�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKOhju  hhubj  )��}�(hhh]�(j  )��}�(h�^.. figure:: edge-scrolling.svg
    :align: center

    Vertical and horizontal edge scrolling
�h]�h}�(h]�h]�h]�h]�h]��uri��edge-scrolling.svg�j  }�j  j�  suhj  hj�  hh,hKVubj  )��}�(h�&Vertical and horizontal edge scrolling�h]�h9�&Vertical and horizontal edge scrolling�����}�(hj�  hj�  ubah}�(h]�h]�h]�h]�h]�uhj  hh,hKVhj�  ubeh}�(h]��id6�ah]�h]�h]�h]�j3  �center�uhj  hKVhju  hhhh,ubhE)��}�(h��Due to the layout of the edges, diagonal scrolling is not possible. The
behavior of edge scrolling using both edges at the same time is undefined.�h]�h9��Due to the layout of the edges, diagonal scrolling is not possible. The
behavior of edge scrolling using both edges at the same time is undefined.�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKXhju  hhubhE)��}�(h�hEdge scrolling overlaps with :ref:`clickpad_softbuttons`. A physical click on
a clickpad ends scrolling.�h]�(h9�Edge scrolling overlaps with �����}�(h�Edge scrolling overlaps with �hj�  hhhNhNubhQ)��}�(h�:ref:`clickpad_softbuttons`�h]�hW)��}�(hj�  h]�h9�clickpad_softbuttons�����}�(hhhj�  ubah}�(h]�h]�(hb�std��std-ref�eh]�h]�h]�uhhVhj�  ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�j�  �refexplicit��hr�clickpad_softbuttons�hthuhv�uhhPhh,hK[hj�  ubh9�0. A physical click on
a clickpad ends scrolling.�����}�(h�0. A physical click on
a clickpad ends scrolling.�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK[hju  hhubh)��}�(h�.. _button_scrolling:�h]�h}�(h]�h]�h]�h]�h]�h*�button-scrolling�uhhhKchju  hhhh,ubeh}�(h]�(ji  �id3�eh]�h]�(�edge scrolling��edge_scrolling�eh]�h]�uhh-hh/hhhh,hKMj�  }�j  j_  sj�  }�ji  j_  subh.)��}�(hhh]�(h3)��}�(h�On-Button scrolling�h]�h9�On-Button scrolling�����}�(hj  hj  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj  hhhh,hKbubhE)��}�(hX(  On-button scrolling converts the motion of a device into scroll events while
a designated button is held down. For example, Lenovo devices provide a
`pointing stick <http://en.wikipedia.org/wiki/Pointing_stick>`_ that emulates
scroll events when the trackstick's middle mouse button is held down.�h]�(h9��On-button scrolling converts the motion of a device into scroll events while
a designated button is held down. For example, Lenovo devices provide a
�����}�(h��On-button scrolling converts the motion of a device into scroll events while
a designated button is held down. For example, Lenovo devices provide a
�hj!  hhhNhNubh �	reference���)��}�(h�?`pointing stick <http://en.wikipedia.org/wiki/Pointing_stick>`_�h]�h9�pointing stick�����}�(hhhj,  ubah}�(h]�h]�h]�h]�h]��name��pointing stick��refuri��+http://en.wikipedia.org/wiki/Pointing_stick�uhj*  hj!  ubh)��}�(h�. <http://en.wikipedia.org/wiki/Pointing_stick>�h]�h}�(h]��pointing-stick�ah]�h]��pointing stick�ah]�h]��refuri�j=  uhh�
referenced�Khj!  ubh9�V that emulates
scroll events when the trackstick’s middle mouse button is held down.�����}�(h�T that emulates
scroll events when the trackstick's middle mouse button is held down.�hj!  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKdhj  hhubh �note���)��}�(h��On-button scrolling is enabled by default for pointing sticks. This
prevents middle-button dragging; all motion events while the middle
button is down are converted to scroll events.�h]�hE)��}�(h��On-button scrolling is enabled by default for pointing sticks. This
prevents middle-button dragging; all motion events while the middle
button is down are converted to scroll events.�h]�h9��On-button scrolling is enabled by default for pointing sticks. This
prevents middle-button dragging; all motion events while the middle
button is down are converted to scroll events.�����}�(hj_  hj]  ubah}�(h]�h]�h]�h]�h]�uhhDhh,hKihjY  ubah}�(h]�h]�h]�h]�h]�uhjW  hj  hhhh,hNubj  )��}�(hhh]�(j  )��}�(h�J.. figure:: button-scrolling.svg
    :align: center

    Button scrolling
�h]�h}�(h]�h]�h]�h]�h]��uri��button-scrolling.svg�j  }�j  j  suhj  hjq  hh,hKpubj  )��}�(h�Button scrolling�h]�h9�Button scrolling�����}�(hj�  hj�  ubah}�(h]�h]�h]�h]�h]�uhj  hh,hKphjq  ubeh}�(h]��id7�ah]�h]�h]�h]�j3  �center�uhj  hKphj  hhhh,ubhE)��}�(hX@  The button may be changed with
**libinput_device_config_scroll_set_button()** but must be on the same device as
the motion events. Cross-device scrolling is not supported but
for one exception: libinput's :ref:`t440_support` enables the use of the middle
button for button scrolling (even when the touchpad is disabled).�h]�(h9�The button may be changed with
�����}�(h�The button may be changed with
�hj�  hhhNhNubjd  )��}�(h�.**libinput_device_config_scroll_set_button()**�h]�h9�*libinput_device_config_scroll_set_button()�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhjc  hj�  ubh9�� but must be on the same device as
the motion events. Cross-device scrolling is not supported but
for one exception: libinput’s �����}�(h�� but must be on the same device as
the motion events. Cross-device scrolling is not supported but
for one exception: libinput's �hj�  hhhNhNubhQ)��}�(h�:ref:`t440_support`�h]�hW)��}�(hj�  h]�h9�t440_support�����}�(hhhj�  ubah}�(h]�h]�(hb�std��std-ref�eh]�h]�h]�uhhVhj�  ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�j�  �refexplicit��hr�t440_support�hthuhv�uhhPhh,hKrhj�  ubh9�` enables the use of the middle
button for button scrolling (even when the touchpad is disabled).�����}�(h�` enables the use of the middle
button for button scrolling (even when the touchpad is disabled).�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKrhj  hhubh)��}�(h�.. _scroll_sources:�h]�h}�(h]�h]�h]�h]�h]�h*�scroll-sources�uhhhK}hj  hhhh,ubeh}�(h]�(�on-button-scrolling�j  eh]�h]�(�on-button scrolling��button_scrolling�eh]�h]�uhh-hh/hhhh,hKbj�  }�j�  j�  sj�  }�j  j�  subh.)��}�(hhh]�(h3)��}�(h�Scroll sources�h]�h9�Scroll sources�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj�  hhhh,hK|ubhE)��}�(hXI  libinput provides a pointer axis *source* for each scroll event. The
source can be obtained with the **libinput_event_pointer_get_axis_source()**
function and is one of **wheel**, **finger**, or **continuous**. The source
information lets a caller decide when to implement kinetic scrolling.
Usually, a caller will process events of source wheel as they come in.
For events of source finger a caller should calculate the velocity of the
scroll motion and upon finger release start a kinetic scrolling motion (i.e.
continue executing a scroll according to some friction factor).
libinput expects the caller to be in charge of widget handling, the source
information is thus enough to provide kinetic scrolling on a per-widget
basis. A caller should cancel kinetic scrolling when the pointer leaves the
current widget or when a key is pressed.�h]�(h9�!libinput provides a pointer axis �����}�(h�!libinput provides a pointer axis �hj  hhhNhNubh �emphasis���)��}�(h�*source*�h]�h9�source�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]�uhj  hj  ubh9�< for each scroll event. The
source can be obtained with the �����}�(h�< for each scroll event. The
source can be obtained with the �hj  hhhNhNubjd  )��}�(h�,**libinput_event_pointer_get_axis_source()**�h]�h9�(libinput_event_pointer_get_axis_source()�����}�(hhhj!  ubah}�(h]�h]�h]�h]�h]�uhjc  hj  ubh9�
function and is one of �����}�(h�
function and is one of �hj  hhhNhNubjd  )��}�(h�	**wheel**�h]�h9�wheel�����}�(hhhj4  ubah}�(h]�h]�h]�h]�h]�uhjc  hj  ubh9�, �����}�(h�, �hj  hhhNhNubjd  )��}�(h�
**finger**�h]�h9�finger�����}�(hhhjG  ubah}�(h]�h]�h]�h]�h]�uhjc  hj  ubh9�, or �����}�(h�, or �hj  hhhNhNubjd  )��}�(h�**continuous**�h]�h9�
continuous�����}�(hhhjZ  ubah}�(h]�h]�h]�h]�h]�uhjc  hj  ubh9Xx  . The source
information lets a caller decide when to implement kinetic scrolling.
Usually, a caller will process events of source wheel as they come in.
For events of source finger a caller should calculate the velocity of the
scroll motion and upon finger release start a kinetic scrolling motion (i.e.
continue executing a scroll according to some friction factor).
libinput expects the caller to be in charge of widget handling, the source
information is thus enough to provide kinetic scrolling on a per-widget
basis. A caller should cancel kinetic scrolling when the pointer leaves the
current widget or when a key is pressed.�����}�(hXx  . The source
information lets a caller decide when to implement kinetic scrolling.
Usually, a caller will process events of source wheel as they come in.
For events of source finger a caller should calculate the velocity of the
scroll motion and upon finger release start a kinetic scrolling motion (i.e.
continue executing a scroll according to some friction factor).
libinput expects the caller to be in charge of widget handling, the source
information is thus enough to provide kinetic scrolling on a per-widget
basis. A caller should cancel kinetic scrolling when the pointer leaves the
current widget or when a key is pressed.�hj  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK~hj�  hhubhE)��}�(h�gSee the **libinput_event_pointer_get_axis_source()** for details on the
behavior of each scroll source.�h]�(h9�See the �����}�(h�See the �hjs  hhhNhNubjd  )��}�(h�,**libinput_event_pointer_get_axis_source()**�h]�h9�(libinput_event_pointer_get_axis_source()�����}�(hhhj|  ubah}�(h]�h]�h]�h]�h]�uhjc  hjs  ubh9�3 for details on the
behavior of each scroll source.�����}�(h�3 for details on the
behavior of each scroll source.�hjs  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj�  hhubhE)��}�(h�JSee also http://who-t.blogspot.com.au/2015/03/libinput-scroll-sources.html�h]�(h9�	See also �����}�(h�	See also �hj�  hhhNhNubj+  )��}�(h�Ahttp://who-t.blogspot.com.au/2015/03/libinput-scroll-sources.html�h]�h9�Ahttp://who-t.blogspot.com.au/2015/03/libinput-scroll-sources.html�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]��refuri�j�  uhj*  hj�  ubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj�  hhubeh}�(h]�(j�  �id4�eh]�h]�(�scroll sources��scroll_sources�eh]�h]�uhh-hh/hhhh,hK|j�  }�j�  j�  sj�  }�j�  j�  subeh}�(h]�(h+�id1�eh]�h]��	scrolling�ah]��	scrolling�ah]�uhh-hhhhhh,hKjK  Kj�  }�j�  h sj�  }�h+h subeh}�(h]�h]�h]�h]�h]��source�h,uhh�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(h2N�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}�(�git_version�h �substitution_definition���)��}�(h�,.. |git_version| replace:: :commit:`424e27f`�h]�j+  )��}�(h�git commit 424e27f�h]�h9�git commit 424e27f�����}�(hhhj*  ubah}�(h]�h]�h]�h]�h]��internal���refuri��?https://gitlab.freedesktop.org/libinput/libinput/commit/424e27f�uhj*  hj&  ubah}�(h]�h]�h]�j#  ah]�h]�uhj$  h�<rst_prolog>�hKhhub�git_version_full�j%  )��}�(h�].. |git_version_full| replace:: :commit:`<function get_git_version_full at 0x7f5e21ad5f28>`

�h]�j+  )��}�(h�<git commit <function get_git_version_full at 0x7f5e21ad5f28>�h]�h9�<git commit <function get_git_version_full at 0x7f5e21ad5f28>�����}�(hhhjG  ubah}�(h]�h]�h]�h]�h]��internal���refuri��ihttps://gitlab.freedesktop.org/libinput/libinput/commit/<function get_git_version_full at 0x7f5e21ad5f28>�uhj*  hjC  ubah}�(h]�h]�h]�jB  ah]�h]�uhj$  hjA  hKhhubu�substitution_names�}�(�git_version�j#  �git_version_full�jB  u�refnames�}��refids�}�(h+]�h aj�  ]�j�  aj�  ]�j�  aji  ]�j_  aj  ]�j�  aj�  ]�j�  au�nameids�}�(j�  h+j�  j�  j�  j�  jp  j�  jo  jl  j  ji  j
  j  j�  j  j�  j�  jG  jD  j�  j�  j�  j�  u�	nametypes�}�(j�  �j�  �j�  Njp  �jo  Nj  �j
  Nj�  �j�  NjG  �j�  �j�  Nuh}�(h+h/j�  h/j�  j�  j�  j�  j�  j�  jl  j�  ji  ju  j  ju  j  j  j�  j  jD  j>  j�  j�  j�  j�  j.  j  j�  j�  j�  jq  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]�h �system_message���)��}�(hhh]�hE)��}�(h�,Duplicate implicit target name: "scrolling".�h]�h9�0Duplicate implicit target name: “scrolling”.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]�j�  a�level�K�type��INFO��source�h,�line�Kuhj�  hh/hhhh,hKuba�transform_messages�]�(j�  )��}�(hhh]�hE)��}�(hhh]�h9�/Hyperlink target "scrolling" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h,�line�Kuhj�  ubj�  )��}�(hhh]�hE)��}�(hhh]�h9�:Hyperlink target "horizontal-scrolling" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h,�line�Kuhj�  ubj�  )��}�(hhh]�hE)��}�(hhh]�h9�9Hyperlink target "twofinger-scrolling" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h,�line�K,uhj�  ubj�  )��}�(hhh]�hE)��}�(hhh]�h9�4Hyperlink target "edge-scrolling" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h,�line�KNuhj�  ubj�  )��}�(hhh]�hE)��}�(hhh]�h9�6Hyperlink target "button-scrolling" is not referenced.�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]�uhhDhj  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h,�line�Kcuhj�  ubj�  )��}�(hhh]�hE)��}�(hhh]�h9�4Hyperlink target "scroll-sources" is not referenced.�����}�(hhhj+  ubah}�(h]�h]�h]�h]�h]�uhhDhj(  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h,�line�K}uhj�  ube�transformer�N�
decoration�Nhhub.