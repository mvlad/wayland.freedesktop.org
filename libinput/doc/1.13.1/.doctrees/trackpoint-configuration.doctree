�� i      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �block_quote���)��}�(hhh]��
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�h	�parent�hhh�source�N�line�Nubh �target���)��}�(h�.. _trackpoint_configuration:�h]�h}�(h]�h]�h]�h]�h]��refid��trackpoint-configuration�uhhhKhhhhh�D/home/whot/code/libinput/build/doc/user/trackpoint-configuration.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�Trackpoint configuration�h]�h �Text����Trackpoint configuration�����}�(hh6hh4hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hh/hhhh,hKubh �	paragraph���)��}�(h��The sections below describe the trackpoint magic multiplier and how to apply
it to your local device. See :ref:`trackpoint_range` for an explanation on
why this multiplier is needed.�h]�(h9�jThe sections below describe the trackpoint magic multiplier and how to apply
it to your local device. See �����}�(h�jThe sections below describe the trackpoint magic multiplier and how to apply
it to your local device. See �hhFhhhNhNub�sphinx.addnodes��pending_xref���)��}�(h�:ref:`trackpoint_range`�h]�h �inline���)��}�(hhTh]�h9�trackpoint_range�����}�(hhhhXubah}�(h]�h]�(�xref��std��std-ref�eh]�h]�h]�uhhVhhRubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�hc�refexplicit���	reftarget��trackpoint_range��refdoc��trackpoint-configuration��refwarn��uhhPhh,hKhhFubh9�5 for an explanation on
why this multiplier is needed.�����}�(h�5 for an explanation on
why this multiplier is needed.�hhFhhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKhh/hhubh �note���)��}�(h��The magic trackpoint multiplier **is not user visible configuration**. It is
part of the :ref:`device-quirks` system and provided once per device.�h]�hE)��}�(h��The magic trackpoint multiplier **is not user visible configuration**. It is
part of the :ref:`device-quirks` system and provided once per device.�h]�(h9� The magic trackpoint multiplier �����}�(h� The magic trackpoint multiplier �hh�ubh �strong���)��}�(h�%**is not user visible configuration**�h]�h9�!is not user visible configuration�����}�(hhhh�ubah}�(h]�h]�h]�h]�h]�uhh�hh�ubh9�. It is
part of the �����}�(h�. It is
part of the �hh�ubhQ)��}�(h�:ref:`device-quirks`�h]�hW)��}�(hh�h]�h9�device-quirks�����}�(hhhh�ubah}�(h]�h]�(hb�std��std-ref�eh]�h]�h]�uhhVhh�ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�h��refexplicit��hr�device-quirks�hthuhv�uhhPhh,hKhh�ubh9�% system and provided once per device.�����}�(h�% system and provided once per device.�hh�ubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKhh�ubah}�(h]�h]�h]�h]�h]�uhh�hh/hhhh,hNubhE)��}�(h�^User-specific preferences can be adjusted with the
:ref:`config_pointer_acceleration` setting.�h]�(h9�3User-specific preferences can be adjusted with the
�����}�(h�3User-specific preferences can be adjusted with the
�hh�hhhNhNubhQ)��}�(h�":ref:`config_pointer_acceleration`�h]�hW)��}�(hh�h]�h9�config_pointer_acceleration�����}�(hhhh�ubah}�(h]�h]�(hb�std��std-ref�eh]�h]�h]�uhhVhh�ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�h�refexplicit��hr�config_pointer_acceleration�hthuhv�uhhPhh,hKhh�ubh9�	 setting.�����}�(h�	 setting.�hh�hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKhh/hhubh)��}�(h�.. _trackpoint_multiplier:�h]�h}�(h]�h]�h]�h]�h]�h*�trackpoint-multiplier�uhhhKhh/hhhh,ubh.)��}�(hhh]�(h3)��}�(h�The magic trackpoint multiplier�h]�h9�The magic trackpoint multiplier�����}�(hj  hj  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj  hhhh,hKubhE)��}�(hX\  To accomodate for the wildly different input data on trackpoint, libinput
uses a multiplier that is applied to input deltas. Trackpoints that send
comparatively high deltas can be "slowed down", trackpoints that send low
deltas can be "sped up" to match the expected range. The actual acceleration
profile is applied to these pre-multiplied deltas.�h]�h9Xd  To accomodate for the wildly different input data on trackpoint, libinput
uses a multiplier that is applied to input deltas. Trackpoints that send
comparatively high deltas can be “slowed down”, trackpoints that send low
deltas can be “sped up” to match the expected range. The actual acceleration
profile is applied to these pre-multiplied deltas.�����}�(hj%  hj#  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKhj  hhubhE)��}�(h��Given a trackpoint delta ``(dx, dy)``, a multiplier ``M`` and a pointer acceleration
function ``f(dx, dy) → (dx', dy')``, the algorithm is effectively:�h]�(h9�Given a trackpoint delta �����}�(h�Given a trackpoint delta �hj1  hhhNhNubh �literal���)��}�(h�``(dx, dy)``�h]�h9�(dx, dy)�����}�(hhhj<  ubah}�(h]�h]�h]�h]�h]�uhj:  hj1  ubh9�, a multiplier �����}�(h�, a multiplier �hj1  hhhNhNubj;  )��}�(h�``M``�h]�h9�M�����}�(hhhjO  ubah}�(h]�h]�h]�h]�h]�uhj:  hj1  ubh9�% and a pointer acceleration
function �����}�(h�% and a pointer acceleration
function �hj1  hhhNhNubj;  )��}�(h�``f(dx, dy) → (dx', dy')``�h]�h9�f(dx, dy) → (dx', dy')�����}�(hhhjb  ubah}�(h]�h]�h]�h]�h]�uhj:  hj1  ubh9�, the algorithm is effectively:�����}�(h�, the algorithm is effectively:�hj1  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKhj  hhubh �literal_block���)��}�(h� f(M * dx, M * dy) → (dx', dy')�h]�h9� f(M * dx, M * dy) → (dx', dy')�����}�(hhhj}  ubah}�(h]�h]�h]�h]�h]��	xml:space��preserve�uhj{  hK'hj  hhhh,ubh)��}�(h�%.. _trackpoint_multiplier_adjustment:�h]�h}�(h]�h]�h]�h]�h]�h*� trackpoint-multiplier-adjustment�uhhhK)hj  hhhh,ubh.)��}�(hhh]�(h3)��}�(h�)Adjusting the magic trackpoint multiplier�h]�h9�)Adjusting the magic trackpoint multiplier�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj�  hhhh,hK(ubhE)��}�(h�This section only applies if:�h]�h9�This section only applies if:�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK*hj�  hhubh �bullet_list���)��}�(hhh]�(h �	list_item���)��}�(h�Ythe trackpoint default speed (speed setting 0) is unusably slow or
unusably fast, **and**�h]�hE)��}�(h�Ythe trackpoint default speed (speed setting 0) is unusably slow or
unusably fast, **and**�h]�(h9�Rthe trackpoint default speed (speed setting 0) is unusably slow or
unusably fast, �����}�(h�Rthe trackpoint default speed (speed setting 0) is unusably slow or
unusably fast, �hj�  ubh�)��}�(h�**and**�h]�h9�and�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK,hj�  ubah}�(h]�h]�h]�h]�h]�uhj�  hj�  hhhh,hNubj�  )��}�(h�kthe lowest speed setting (-1) is still too fast **or** the highest speed
setting is still too slow, **and**�h]�hE)��}�(h�kthe lowest speed setting (-1) is still too fast **or** the highest speed
setting is still too slow, **and**�h]�(h9�0the lowest speed setting (-1) is still too fast �����}�(h�0the lowest speed setting (-1) is still too fast �hj�  ubh�)��}�(h�**or**�h]�h9�or�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh9�. the highest speed
setting is still too slow, �����}�(h�. the highest speed
setting is still too slow, �hj�  ubh�)��}�(h�**and**�h]�h9�and�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK.hj�  ubah}�(h]�h]�h]�h]�h]�uhj�  hj�  hhhh,hNubj�  )��}�(h�rthe :ref:`device-quirks` for this device do not list a trackpoint multiplier
(see :ref:`device-quirks-debugging`)
�h]�hE)��}�(h�qthe :ref:`device-quirks` for this device do not list a trackpoint multiplier
(see :ref:`device-quirks-debugging`)�h]�(h9�the �����}�(h�the �hj#  ubhQ)��}�(h�:ref:`device-quirks`�h]�hW)��}�(hj.  h]�h9�device-quirks�����}�(hhhj0  ubah}�(h]�h]�(hb�std��std-ref�eh]�h]�h]�uhhVhj,  ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�j:  �refexplicit��hr�device-quirks�hthuhv�uhhPhh,hK0hj#  ubh9�: for this device do not list a trackpoint multiplier
(see �����}�(h�: for this device do not list a trackpoint multiplier
(see �hj#  ubhQ)��}�(h�:ref:`device-quirks-debugging`�h]�hW)��}�(hjQ  h]�h9�device-quirks-debugging�����}�(hhhjS  ubah}�(h]�h]�(hb�std��std-ref�eh]�h]�h]�uhhVhjO  ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�j]  �refexplicit��hr�device-quirks-debugging�hthuhv�uhhPhh,hK0hj#  ubh9�)�����}�(h�)�hj#  ubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK0hj  ubah}�(h]�h]�h]�h]�h]�uhj�  hj�  hhhh,hNubeh}�(h]�h]�h]�h]�h]��bullet��-�uhj�  hh,hK,hj�  hhubhE)��}�(h�qIf the only satisfactory speed settings are less than -0.75 or greater than
0.75, a multiplier *may* be required.�h]�(h9�_If the only satisfactory speed settings are less than -0.75 or greater than
0.75, a multiplier �����}�(h�_If the only satisfactory speed settings are less than -0.75 or greater than
0.75, a multiplier �hj�  hhhNhNubh �emphasis���)��}�(h�*may*�h]�h9�may�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhj�  hj�  ubh9� be required.�����}�(h� be required.�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK3hj�  hhubhE)��}�(hXD  A specific multiplier will apply to **all users with the same laptop
model**, so proceed with caution. You must be capable/willing to adjust
device quirks, build libinput from source and restart the session frequently
to adjust the multiplier. If this does not apply, wait for someone else with
the same hardware to do this.�h]�(h9�$A specific multiplier will apply to �����}�(h�$A specific multiplier will apply to �hj�  hhhNhNubh�)��}�(h�(**all users with the same laptop
model**�h]�h9�$all users with the same laptop
model�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh9��, so proceed with caution. You must be capable/willing to adjust
device quirks, build libinput from source and restart the session frequently
to adjust the multiplier. If this does not apply, wait for someone else with
the same hardware to do this.�����}�(h��, so proceed with caution. You must be capable/willing to adjust
device quirks, build libinput from source and restart the session frequently
to adjust the multiplier. If this does not apply, wait for someone else with
the same hardware to do this.�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK6hj�  hhubhE)��}�(h��Finding the correct multiplier is difficult and requires some trial and
error. The default multiplier is always 1.0. A value between 0.0 and 1.0
slows the trackpoint down, a value above 1.0 speeds the trackpoint up.
Values below zero are invalid.�h]�h9��Finding the correct multiplier is difficult and requires some trial and
error. The default multiplier is always 1.0. A value between 0.0 and 1.0
slows the trackpoint down, a value above 1.0 speeds the trackpoint up.
Values below zero are invalid.�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK<hj�  hhubh �warning���)��}�(h��The multiplier is not a configuration to adjust to personal
preferences. The multiplier normalizes the input data into a range that
can then be configured with the speed setting.�h]�hE)��}�(h��The multiplier is not a configuration to adjust to personal
preferences. The multiplier normalizes the input data into a range that
can then be configured with the speed setting.�h]�h9��The multiplier is not a configuration to adjust to personal
preferences. The multiplier normalizes the input data into a range that
can then be configured with the speed setting.�����}�(hj�  hj�  ubah}�(h]�h]�h]�h]�h]�uhhDhh,hKAhj�  ubah}�(h]�h]�h]�h]�h]�uhj�  hj�  hhhh,hNubhE)��}�(h��To adjust the local multiplier, first
:ref:`build libinput from git master <building_libinput>`. It is not
required to install libinput from git. The below assumes that all
:ref:`building_dependencies` are already
installed.�h]�(h9�&To adjust the local multiplier, first
�����}�(h�&To adjust the local multiplier, first
�hj�  hhhNhNubhQ)��}�(h�9:ref:`build libinput from git master <building_libinput>`�h]�hW)��}�(hj�  h]�h9�build libinput from git master�����}�(hhhj  ubah}�(h]�h]�(hb�std��std-ref�eh]�h]�h]�uhhVhj�  ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�j  �refexplicit��hr�building_libinput�hthuhv�uhhPhh,hKEhj�  ubh9�N. It is not
required to install libinput from git. The below assumes that all
�����}�(h�N. It is not
required to install libinput from git. The below assumes that all
�hj�  hhhNhNubhQ)��}�(h�:ref:`building_dependencies`�h]�hW)��}�(hj"  h]�h9�building_dependencies�����}�(hhhj$  ubah}�(h]�h]�(hb�std��std-ref�eh]�h]�h]�uhhVhj   ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�j.  �refexplicit��hr�building_dependencies�hthuhv�uhhPhh,hKEhj�  ubh9� are already
installed.�����}�(h� are already
installed.�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKEhj�  hhubj|  )��}�(hX  $ cd path/to/libinput.git

# Use an approximate multiplier in the quirks file
$ cat > quirks/99-trackpont-override.quirks <<EOF
[Trackpoint Override]
MatchUdevType=pointingstick
AttrTrackpointMultiplier=1.0
EOF

# Use your trackpoint's event node. If the Attr does not show up
# then the quirk does not apply to your trackpoint.
$ ./builddir/libinput quirks list /dev/input/event18
AttrTrackpointMultiplier=1.0

# Now start a GUI program to debug the trackpoint speed.
# ESC closes the debug GUI
$ sudo ./builddir/libinput debug-gui�h]�h9X  $ cd path/to/libinput.git

# Use an approximate multiplier in the quirks file
$ cat > quirks/99-trackpont-override.quirks <<EOF
[Trackpoint Override]
MatchUdevType=pointingstick
AttrTrackpointMultiplier=1.0
EOF

# Use your trackpoint's event node. If the Attr does not show up
# then the quirk does not apply to your trackpoint.
$ ./builddir/libinput quirks list /dev/input/event18
AttrTrackpointMultiplier=1.0

# Now start a GUI program to debug the trackpoint speed.
# ESC closes the debug GUI
$ sudo ./builddir/libinput debug-gui�����}�(hhhjI  ubah}�(h]�h]�h]�h]�h]�j�  j�  uhj{  hKShj�  hhhh,ubhE)��}�(hXd  Replace the multiplier with an approximate value and the event node with
your trackpoint's event node. Try to use trackpoint and verify the
multiplier is good enough. If not, adjust the ``.quirks`` file and re-run the
``libinput debug-gui``.  Note that the ``libinput debug-gui`` always feels
less responsive than libinput would behave in a normal install.�h]�(h9��Replace the multiplier with an approximate value and the event node with
your trackpoint’s event node. Try to use trackpoint and verify the
multiplier is good enough. If not, adjust the �����}�(h��Replace the multiplier with an approximate value and the event node with
your trackpoint's event node. Try to use trackpoint and verify the
multiplier is good enough. If not, adjust the �hjW  hhhNhNubj;  )��}�(h�``.quirks``�h]�h9�.quirks�����}�(hhhj`  ubah}�(h]�h]�h]�h]�h]�uhj:  hjW  ubh9� file and re-run the
�����}�(h� file and re-run the
�hjW  hhhNhNubj;  )��}�(h�``libinput debug-gui``�h]�h9�libinput debug-gui�����}�(hhhjs  ubah}�(h]�h]�h]�h]�h]�uhj:  hjW  ubh9�.  Note that the �����}�(h�.  Note that the �hjW  hhhNhNubj;  )��}�(h�``libinput debug-gui``�h]�h9�libinput debug-gui�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhj:  hjW  ubh9�M always feels
less responsive than libinput would behave in a normal install.�����}�(h�M always feels
less responsive than libinput would behave in a normal install.�hjW  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKahj�  hhubhE)��}�(h�POnce the trackpoint behaves correctly you are ready to test the system
libinput:�h]�h9�POnce the trackpoint behaves correctly you are ready to test the system
libinput:�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKghj�  hhubj|  )��}�(h�S$ sudo cp quirks/99-trackpoint-override.quirks /etc/libinput/local-overrides.quirks�h]�h9�S$ sudo cp quirks/99-trackpoint-override.quirks /etc/libinput/local-overrides.quirks�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�j�  j�  uhj{  hKrhj�  hhhh,ubhE)��}�(h�6Now verify the override is seen by the system libinput�h]�h9�6Now verify the override is seen by the system libinput�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKphj�  hhubj|  )��}�(h�3$ libinput quirks list
AttrTrackpointMultiplier=1.0�h]�h9�3$ libinput quirks list
AttrTrackpointMultiplier=1.0�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�j�  j�  uhj{  hKyhj�  hhhh,ubhE)��}�(h�|If the multiplier is listed, restart your Wayland session or X server. The
new multiplier is now applied to your trackpoint.�h]�h9�|If the multiplier is listed, restart your Wayland session or X server. The
new multiplier is now applied to your trackpoint.�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKxhj�  hhubhE)��}�(hX  If the trackpoint behavior is acceptable, you are ready to submit this file
upstream. First, find add a more precise match for the device so it only
applies to the built-in trackpoint on your laptop model. Usually a
variation of the following is sufficient:�h]�h9X  If the trackpoint behavior is acceptable, you are ready to submit this file
upstream. First, find add a more precise match for the device so it only
applies to the built-in trackpoint on your laptop model. Usually a
variation of the following is sufficient:�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK{hj�  hhubj|  )��}�(h��[Trackpoint Override]
MatchUdevType=pointingstick
MatchName=*TPPS/2 IBM TrackPoint*
MatchDMIModalias=dmi:*svnLENOVO:*:pvrThinkPadT440p*
AttrTrackpointMultiplier=1.0�h]�h9��[Trackpoint Override]
MatchUdevType=pointingstick
MatchName=*TPPS/2 IBM TrackPoint*
MatchDMIModalias=dmi:*svnLENOVO:*:pvrThinkPadT440p*
AttrTrackpointMultiplier=1.0�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�j�  j�  uhj{  hK�hj�  hhhh,ubhE)��}�(hX3  Look at your ``/sys/class/dmi/id/modalias`` file for the values to add. Verify
that ``libinput quirks list`` still shows the ``AttrTrackpointMultiplier``. If
it does, then you should :ref:`report a bug <reporting_bugs>` with the contents of
the file. Alternatively, file a merge request with the data added.�h]�(h9�Look at your �����}�(h�Look at your �hj  hhhNhNubj;  )��}�(h�``/sys/class/dmi/id/modalias``�h]�h9�/sys/class/dmi/id/modalias�����}�(hhhj
  ubah}�(h]�h]�h]�h]�h]�uhj:  hj  ubh9�) file for the values to add. Verify
that �����}�(h�) file for the values to add. Verify
that �hj  hhhNhNubj;  )��}�(h�``libinput quirks list``�h]�h9�libinput quirks list�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]�uhj:  hj  ubh9� still shows the �����}�(h� still shows the �hj  hhhNhNubj;  )��}�(h�``AttrTrackpointMultiplier``�h]�h9�AttrTrackpointMultiplier�����}�(hhhj0  ubah}�(h]�h]�h]�h]�h]�uhj:  hj  ubh9�. If
it does, then you should �����}�(h�. If
it does, then you should �hj  hhhNhNubhQ)��}�(h�$:ref:`report a bug <reporting_bugs>`�h]�hW)��}�(hjE  h]�h9�report a bug�����}�(hhhjG  ubah}�(h]�h]�(hb�std��std-ref�eh]�h]�h]�uhhVhjC  ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�jQ  �refexplicit��hr�reporting_bugs�hthuhv�uhhPhh,hK�hj  ubh9�X with the contents of
the file. Alternatively, file a merge request with the data added.�����}�(h�X with the contents of
the file. Alternatively, file a merge request with the data added.�hj  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj�  hhubh)��}�(h�.. _trackpoint_range_measure:�h]�h}�(h]�h]�h]�h]�h]�h*�trackpoint-range-measure�uhhhK�hj�  hhhh,ubeh}�(h]�(�)adjusting-the-magic-trackpoint-multiplier�j�  eh]�h]�(�)adjusting the magic trackpoint multiplier�� trackpoint_multiplier_adjustment�eh]�h]�uhh-hj  hhhh,hK(�expect_referenced_by_name�}�j}  j�  s�expect_referenced_by_id�}�j�  j�  subeh}�(h]�(�the-magic-trackpoint-multiplier�j  eh]�h]�(�the magic trackpoint multiplier��trackpoint_multiplier�eh]�h]�uhh-hh/hhhh,hKj�  }�j�  j  sj�  }�j  j  subh.)��}�(hhh]�(h3)��}�(h�Measuring the trackpoint range�h]�h9�Measuring the trackpoint range�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj�  hhhh,hK�ubhE)��}�(h��This section only applied to libinput version 1.9.x, 1.10.x, and 1.11.x and
has been removed. See :ref:`trackpoint_multiplier` for versions 1.12.x and later.�h]�(h9�bThis section only applied to libinput version 1.9.x, 1.10.x, and 1.11.x and
has been removed. See �����}�(h�bThis section only applied to libinput version 1.9.x, 1.10.x, and 1.11.x and
has been removed. See �hj�  hhhNhNubhQ)��}�(h�:ref:`trackpoint_multiplier`�h]�hW)��}�(hj�  h]�h9�trackpoint_multiplier�����}�(hhhj�  ubah}�(h]�h]�(hb�std��std-ref�eh]�h]�h]�uhhVhj�  ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�j�  �refexplicit��hr�trackpoint_multiplier�hthuhv�uhhPhh,hK�hj�  ubh9� for versions 1.12.x and later.�����}�(h� for versions 1.12.x and later.�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj�  hhubhE)��}�(h��If using libinput version 1.11.x or earlier, please see
`the 1.11.0 documentation <https://wayland.freedesktop.org/libinput/doc/1.11.0/trackpoints.html#trackpoint_range_measure>`_�h]�(h9�8If using libinput version 1.11.x or earlier, please see
�����}�(h�8If using libinput version 1.11.x or earlier, please see
�hj�  hhhNhNubh �	reference���)��}�(h�{`the 1.11.0 documentation <https://wayland.freedesktop.org/libinput/doc/1.11.0/trackpoints.html#trackpoint_range_measure>`_�h]�h9�the 1.11.0 documentation�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]��name��the 1.11.0 documentation��refuri��]https://wayland.freedesktop.org/libinput/doc/1.11.0/trackpoints.html#trackpoint_range_measure�uhj�  hj�  ubh)��}�(h�` <https://wayland.freedesktop.org/libinput/doc/1.11.0/trackpoints.html#trackpoint_range_measure>�h]�h}�(h]��the-1-11-0-documentation�ah]�h]��the 1.11.0 documentation�ah]�h]��refuri�j�  uhh�
referenced�Khj�  ubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj�  hhubeh}�(h]�(�measuring-the-trackpoint-range�jv  eh]�h]�(�measuring the trackpoint range��trackpoint_range_measure�eh]�h]�uhh-hh/hhhh,hK�j�  }�j	  jl  sj�  }�jv  jl  subeh}�(h]�(h+�id1�eh]�h]�(�trackpoint configuration��trackpoint_configuration�eh]�h]�uhh-hhhhhh,hKj�  }�j  h sj�  }�h+h subeh}�(h]�h]�h]�h]�h]��source�h,uhh�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(h2N�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j<  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}�(�git_version�h �substitution_definition���)��}�(h�,.. |git_version| replace:: :commit:`424e27f`�h]�j�  )��}�(h�git commit 424e27f�h]�h9�git commit 424e27f�����}�(hhhjz  ubah}�(h]�h]�h]�h]�h]��internal���refuri��?https://gitlab.freedesktop.org/libinput/libinput/commit/424e27f�uhj�  hjv  ubah}�(h]�h]�h]�js  ah]�h]�uhjt  h�<rst_prolog>�hKhhub�git_version_full�ju  )��}�(h�].. |git_version_full| replace:: :commit:`<function get_git_version_full at 0x7f5e21ad5f28>`

�h]�j�  )��}�(h�<git commit <function get_git_version_full at 0x7f5e21ad5f28>�h]�h9�<git commit <function get_git_version_full at 0x7f5e21ad5f28>�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]��internal���refuri��ihttps://gitlab.freedesktop.org/libinput/libinput/commit/<function get_git_version_full at 0x7f5e21ad5f28>�uhj�  hj�  ubah}�(h]�h]�h]�j�  ah]�h]�uhjt  hj�  hKhhubu�substitution_names�}�(�git_version�js  �git_version_full�j�  u�refnames�}��refids�}�(h+]�h aj  ]�j  aj�  ]�j�  ajv  ]�jl  au�nameids�}�(j  h+j  j  j�  j  j�  j�  j}  j�  j|  jy  j	  jv  j  j  j�  j�  u�	nametypes�}�(j  �j  Nj�  �j�  Nj}  �j|  Nj	  �j  Nj�  �uh}�(h+h/j  h/j  j  j�  j  j�  j�  jy  j�  jv  j�  j  j�  j�  j�  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]��transform_messages�]�(h �system_message���)��}�(hhh]�hE)��}�(hhh]�h9�>Hyperlink target "trackpoint-configuration" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type��INFO��source�h,�line�Kuhj�  ubj�  )��}�(hhh]�hE)��}�(hhh]�h9�;Hyperlink target "trackpoint-multiplier" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h,�line�Kuhj�  ubj�  )��}�(hhh]�hE)��}�(hhh]�h9�FHyperlink target "trackpoint-multiplier-adjustment" is not referenced.�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]�uhhDhj  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h,�line�K)uhj�  ubj�  )��}�(hhh]�hE)��}�(hhh]�h9�>Hyperlink target "trackpoint-range-measure" is not referenced.�����}�(hhhj*  ubah}�(h]�h]�h]�h]�h]�uhhDhj'  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h,�line�K�uhj�  ube�transformer�N�
decoration�Nhhub.