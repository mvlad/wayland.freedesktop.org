��F$      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �block_quote���)��}�(hhh]��
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�h	�parent�hhh�source�N�line�Nubh �target���)��}�(h�.. _timestamps:�h]�h}�(h]�h]�h]�h]�h]��refid��
timestamps�uhhhKhhhhh�6/home/whot/code/libinput/build/doc/user/timestamps.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�
Timestamps�h]�h �Text����
Timestamps�����}�(hh6hh4hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hh/hhhh,hKubh)��}�(h�.. _event_timestamps:�h]�h}�(h]�h]�h]�h]�h]�h*�event-timestamps�uhhhKhh/hhhh,ubh.)��}�(hhh]�(h3)��}�(h�Event timestamps�h]�h9�Event timestamps�����}�(hhThhRhhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hhOhhhh,hKubh �	paragraph���)��}�(hX&  Most libinput events provide a timestamp in millisecond and/or microsecond
resolution. These timestamp usually increase monotonically, but libinput
does not guarantee that this always the case. In other words, it is possible
to receive an event with a timestamp earlier than the previous event.�h]�h9X&  Most libinput events provide a timestamp in millisecond and/or microsecond
resolution. These timestamp usually increase monotonically, but libinput
does not guarantee that this always the case. In other words, it is possible
to receive an event with a timestamp earlier than the previous event.�����}�(hhdhhbhhhNhNubah}�(h]�h]�h]�h]�h]�uhh`hh,hKhhOhhubha)��}�(h��For example, if a touchpad has :ref:`tapping` enabled, a button event may have a
lower timestamp than an event from a different device. Tapping requires the
use of timeouts to detect multi-finger taps and/or :ref:`tapndrag`.�h]�(h9�For example, if a touchpad has �����}�(h�For example, if a touchpad has �hhphhhNhNub�sphinx.addnodes��pending_xref���)��}�(h�:ref:`tapping`�h]�h �inline���)��}�(hh~h]�h9�tapping�����}�(hhhh�ubah}�(h]�h]�(�xref��std��std-ref�eh]�h]�h]�uhh�hh|ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�h��refexplicit���	reftarget��tapping��refdoc��
timestamps��refwarn��uhhzhh,hKhhpubh9�� enabled, a button event may have a
lower timestamp than an event from a different device. Tapping requires the
use of timeouts to detect multi-finger taps and/or �����}�(h�� enabled, a button event may have a
lower timestamp than an event from a different device. Tapping requires the
use of timeouts to detect multi-finger taps and/or �hhphhhNhNubh{)��}�(h�:ref:`tapndrag`�h]�h�)��}�(hh�h]�h9�tapndrag�����}�(hhhh�ubah}�(h]�h]�(h��std��std-ref�eh]�h]�h]�uhh�hh�ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�h��refexplicit��h��tapndrag�h�h�h��uhhzhh,hKhhpubh9�.�����}�(h�.�hhphhhNhNubeh}�(h]�h]�h]�h]�h]�uhh`hh,hKhhOhhubha)��}�(h�CConsider the following event sequences from a touchpad and a mouse:�h]�h9�CConsider the following event sequences from a touchpad and a mouse:�����}�(hh�hh�hhhNhNubah}�(h]�h]�h]�h]�h]�uhh`hh,hKhhOhhubh �literal_block���)��}�(h��Time      Touchpad      Mouse
---------------------------------
t1       finger down
t2        finger up
t3                     movement
t4       tap timeout�h]�h9��Time      Touchpad      Mouse
---------------------------------
t1       finger down
t2        finger up
t3                     movement
t4       tap timeout�����}�(hhhh�ubah}�(h]�h]�h]�h]�h]��	xml:space��preserve�uhh�hK!hhOhhhh,ubha)��}�(hXz  For this event sequence, the first event to be sent to a caller is in
response to the mouse movement: an event of type
**LIBINPUT_EVENT_POINTER_MOTION** with the timestamp t3.
Once the timeout expires at t4, libinput generates an event of
**LIBINPUT_EVENT_POINTER_BUTTON** (press) with a timestamp t1 and an event
**LIBINPUT_EVENT_POINTER_BUTTON** (release) with a timestamp t2.�h]�(h9�wFor this event sequence, the first event to be sent to a caller is in
response to the mouse movement: an event of type
�����}�(h�wFor this event sequence, the first event to be sent to a caller is in
response to the mouse movement: an event of type
�hh�hhhNhNubh �strong���)��}�(h�!**LIBINPUT_EVENT_POINTER_MOTION**�h]�h9�LIBINPUT_EVENT_POINTER_MOTION�����}�(hhhh�ubah}�(h]�h]�h]�h]�h]�uhh�hh�ubh9�W with the timestamp t3.
Once the timeout expires at t4, libinput generates an event of
�����}�(h�W with the timestamp t3.
Once the timeout expires at t4, libinput generates an event of
�hh�hhhNhNubh�)��}�(h�!**LIBINPUT_EVENT_POINTER_BUTTON**�h]�h9�LIBINPUT_EVENT_POINTER_BUTTON�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]�uhh�hh�ubh9�* (press) with a timestamp t1 and an event
�����}�(h�* (press) with a timestamp t1 and an event
�hh�hhhNhNubh�)��}�(h�!**LIBINPUT_EVENT_POINTER_BUTTON**�h]�h9�LIBINPUT_EVENT_POINTER_BUTTON�����}�(hhhj   ubah}�(h]�h]�h]�h]�h]�uhh�hh�ubh9� (release) with a timestamp t2.�����}�(h� (release) with a timestamp t2.�hh�hhhNhNubeh}�(h]�h]�h]�h]�h]�uhh`hh,hK$hhOhhubha)��}�(h�[Thus, the caller gets events with timestamps in the order t3, t1, t2,
despite t3 > t2 > t1.�h]�h9�[Thus, the caller gets events with timestamps in the order t3, t1, t2,
despite t3 > t2 > t1.�����}�(hj;  hj9  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh`hh,hK+hhOhhubeh}�(h]�(hN�id2�eh]�h]�(�event timestamps��event_timestamps�eh]�h]�uhh-hh/hhhh,hK�expect_referenced_by_name�}�jM  hDs�expect_referenced_by_id�}�hNhDsubeh}�(h]�(h+�id1�eh]�h]��
timestamps�ah]��
timestamps�ah]�uhh-hhhhhh,hK�
referenced�KjP  }�jY  h sjR  }�h+h subeh}�(h]�h]�h]�h]�h]��source�h,uhh�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(h2N�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}�(�git_version�h �substitution_definition���)��}�(h�,.. |git_version| replace:: :commit:`3325b81`�h]�h �	reference���)��}�(h�git commit 3325b81�h]�h9�git commit 3325b81�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]��internal���refuri��?https://gitlab.freedesktop.org/libinput/libinput/commit/3325b81�uhj�  hj�  ubah}�(h]�h]�h]�j�  ah]�h]�uhj�  h�<rst_prolog>�hKhhub�git_version_full�j�  )��}�(h�^.. |git_version_full| replace:: :commit:`<function get_git_version_full at 0x7f4db2896f28>`


�h]�j�  )��}�(h�<git commit <function get_git_version_full at 0x7f4db2896f28>�h]�h9�<git commit <function get_git_version_full at 0x7f4db2896f28>�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]��internal���refuri��ihttps://gitlab.freedesktop.org/libinput/libinput/commit/<function get_git_version_full at 0x7f4db2896f28>�uhj�  hj�  ubah}�(h]�h]�h]�j�  ah]�h]�uhj�  hj�  hKhhubu�substitution_names�}�(�git_version�j�  �git_version_full�j�  u�refnames�}��refids�}�(h+]�h ahN]�hDau�nameids�}�(jY  h+jM  hNjL  jI  u�	nametypes�}�(jY  �jM  �jL  Nuh}�(h+h/jV  h/hNhOjI  hOu�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]�h �system_message���)��}�(hhh]�ha)��}�(h�-Duplicate implicit target name: "timestamps".�h]�h9�1Duplicate implicit target name: “timestamps”.�����}�(hhhj   ubah}�(h]�h]�h]�h]�h]�uhh`hj  ubah}�(h]�h]�h]�h]�h]�jV  a�level�K�type��INFO��source�h,�line�Kuhj  hh/hhhh,hKuba�transform_messages�]�(j  )��}�(hhh]�ha)��}�(hhh]�h9�0Hyperlink target "timestamps" is not referenced.�����}�(hhhj>  ubah}�(h]�h]�h]�h]�h]�uhh`hj;  ubah}�(h]�h]�h]�h]�h]��level�K�type�j6  �source�h,�line�Kuhj  ubj  )��}�(hhh]�ha)��}�(hhh]�h9�6Hyperlink target "event-timestamps" is not referenced.�����}�(hhhjX  ubah}�(h]�h]�h]�h]�h]�uhh`hjU  ubah}�(h]�h]�h]�h]�h]��level�K�type�j6  �source�h,�line�Kuhj  ube�transformer�N�
decoration�Nhhub.