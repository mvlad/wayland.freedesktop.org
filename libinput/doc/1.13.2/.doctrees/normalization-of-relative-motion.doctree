��@      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �block_quote���)��}�(hhh]��
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�h	�parent�hhh�source�N�line�Nubh �target���)��}�(h�.. _motion_normalization:�h]�h}�(h]�h]�h]�h]�h]��refid��motion-normalization�uhhhKhhhhh�L/home/whot/code/libinput/build/doc/user/normalization-of-relative-motion.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h� Normalization of relative motion�h]�h �Text���� Normalization of relative motion�����}�(hh6hh4hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hh/hhhh,hKubh �	paragraph���)��}�(hX  Most relative input devices generate input in so-called "mickeys". A
mickey is in device-specific units that depend on the resolution
of the sensor. Most optical mice use sensors with 1000dpi resolution, but
some devices range from 100dpi to well above 8000dpi.�h]�h9X	  Most relative input devices generate input in so-called “mickeys”. A
mickey is in device-specific units that depend on the resolution
of the sensor. Most optical mice use sensors with 1000dpi resolution, but
some devices range from 100dpi to well above 8000dpi.�����}�(hhHhhFhhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKhh/hhubhE)��}�(hX*  Without a physical reference point, a relative coordinate cannot be
interpreted correctly. A delta of 10 mickeys may be a millimeter of
physical movement or 10 millimeters, depending on the sensor. This
affects pointer acceleration in libinput and interpretation of relative
coordinates in callers.�h]�h9X*  Without a physical reference point, a relative coordinate cannot be
interpreted correctly. A delta of 10 mickeys may be a millimeter of
physical movement or 10 millimeters, depending on the sensor. This
affects pointer acceleration in libinput and interpretation of relative
coordinates in callers.�����}�(hhVhhThhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKhh/hhubhE)��}�(hX  libinput does partial normalization of relative input. For devices with a
resolution of 1000dpi and higher, motion events are normalized to a default
of 1000dpi before pointer acceleration is applied. As a result, devices with
1000dpi and above feel the same.�h]�h9X  libinput does partial normalization of relative input. For devices with a
resolution of 1000dpi and higher, motion events are normalized to a default
of 1000dpi before pointer acceleration is applied. As a result, devices with
1000dpi and above feel the same.�����}�(hhdhhbhhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKhh/hhubhE)��}�(hX�  Devices below 1000dpi are not normalized (normalization of a 1-device unit
movement on a 400dpi mouse would cause a 2.5 pixel movement). Instead,
libinput applies a dpi-dependent acceleration function. At low speeds, a
1-device unit movement usually translates into a 1-pixel movements. As the
movement speed increases, acceleration is applied - at high speeds a low-dpi
device will roughly feel the same as a higher-dpi mouse.�h]�h9X�  Devices below 1000dpi are not normalized (normalization of a 1-device unit
movement on a 400dpi mouse would cause a 2.5 pixel movement). Instead,
libinput applies a dpi-dependent acceleration function. At low speeds, a
1-device unit movement usually translates into a 1-pixel movements. As the
movement speed increases, acceleration is applied - at high speeds a low-dpi
device will roughly feel the same as a higher-dpi mouse.�����}�(hhrhhphhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKhh/hhubhE)��}�(h��This normalization only applies to accelerated coordinates, unaccelerated
coordinates are left in device-units. It is up to the caller to interpret
those coordinates correctly.�h]�h9��This normalization only applies to accelerated coordinates, unaccelerated
coordinates are left in device-units. It is up to the caller to interpret
those coordinates correctly.�����}�(hh�hh~hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKhh/hhubh)��}�(h�".. _motion_normalization_touchpad:�h]�h}�(h]�h]�h]�h]�h]�h*�motion-normalization-touchpad�uhhhK'hh/hhhh,ubh.)��}�(hhh]�(h3)��}�(h�%Normalization of touchpad coordinates�h]�h9�%Normalization of touchpad coordinates�����}�(hh�hh�hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hh�hhhh,hK&ubhE)��}�(h��Touchpads may have a different resolution for the horizontal and vertical
axis. Interpreting coordinates from the touchpad without taking resolution
into account results in uneven motion.�h]�h9��Touchpads may have a different resolution for the horizontal and vertical
axis. Interpreting coordinates from the touchpad without taking resolution
into account results in uneven motion.�����}�(hh�hh�hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK(hh�hhubhE)��}�(h��libinput scales unaccelerated touchpad motion to the resolution of the
touchpad's x axis, i.e. the unaccelerated value for the y axis is:
``y = (x / resolution_x) * resolution_y``.�h]�(h9��libinput scales unaccelerated touchpad motion to the resolution of the
touchpad’s x axis, i.e. the unaccelerated value for the y axis is:
�����}�(h��libinput scales unaccelerated touchpad motion to the resolution of the
touchpad's x axis, i.e. the unaccelerated value for the y axis is:
�hh�hhhNhNubh �literal���)��}�(h�)``y = (x / resolution_x) * resolution_y``�h]�h9�%y = (x / resolution_x) * resolution_y�����}�(hhhh�ubah}�(h]�h]�h]�h]�h]�uhh�hh�ubh9�.�����}�(h�.�hh�hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK,hh�hhubh)��}�(h� .. _motion_normalization_tablet:�h]�h}�(h]�h]�h]�h]�h]�h*�motion-normalization-tablet�uhhhK5hh�hhhh,ubeh}�(h]�(�%normalization-of-touchpad-coordinates�h�eh]�h]�(�%normalization of touchpad coordinates��motion_normalization_touchpad�eh]�h]�uhh-hh/hhhh,hK&�expect_referenced_by_name�}�h�h�s�expect_referenced_by_id�}�h�h�subh.)��}�(hhh]�(h3)��}�(h�#Normalization of tablet coordinates�h]�h9�#Normalization of tablet coordinates�����}�(hh�hh�hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hh�hhhh,hK4ubhE)��}�(h�!See :ref:`tablet-relative-motion`�h]�(h9�See �����}�(h�See �hj  hhhNhNub�sphinx.addnodes��pending_xref���)��}�(h�:ref:`tablet-relative-motion`�h]�h �inline���)��}�(hj  h]�h9�tablet-relative-motion�����}�(hhhj  ubah}�(h]�h]�(�xref��std��std-ref�eh]�h]�h]�uhj  hj  ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�j   �refexplicit���	reftarget��tablet-relative-motion��refdoc�� normalization-of-relative-motion��refwarn��uhj  hh,hK6hj  ubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK6hh�hhubh)��}�(h�'.. _motion_normalization_customization:�h]�h}�(h]�h]�h]�h]�h]�h*�"motion-normalization-customization�uhhhK=hh�hhhh,ubeh}�(h]�(�#normalization-of-tablet-coordinates�h�eh]�h]�(�#normalization of tablet coordinates��motion_normalization_tablet�eh]�h]�uhh-hh/hhhh,hK4h�}�jK  h�sh�}�h�h�subh.)��}�(hhh]�(h3)��}�(h�Setting custom DPI settings�h]�h9�Setting custom DPI settings�����}�(hjU  hjS  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hjP  hhhh,hK<ubhE)��}�(hX�  Devices usually do not advertise their resolution and libinput relies on
the udev property **MOUSE_DPI** for this information. This property is usually
set via the
`udev hwdb <http://cgit.freedesktop.org/systemd/systemd/tree/hwdb/70-mouse.hwdb>`_.
The ``mouse-dpi-tool`` utility provided by
`libevdev <https://freedesktop.org/wiki/Software/libevdev/>`_ should be
used to measure a device's resolution.�h]�(h9�[Devices usually do not advertise their resolution and libinput relies on
the udev property �����}�(h�[Devices usually do not advertise their resolution and libinput relies on
the udev property �hja  hhhNhNubh �strong���)��}�(h�**MOUSE_DPI**�h]�h9�	MOUSE_DPI�����}�(hhhjl  ubah}�(h]�h]�h]�h]�h]�uhjj  hja  ubh9�< for this information. This property is usually
set via the
�����}�(h�< for this information. This property is usually
set via the
�hja  hhhNhNubh �	reference���)��}�(h�R`udev hwdb <http://cgit.freedesktop.org/systemd/systemd/tree/hwdb/70-mouse.hwdb>`_�h]�h9�	udev hwdb�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]��name��	udev hwdb��refuri��Chttp://cgit.freedesktop.org/systemd/systemd/tree/hwdb/70-mouse.hwdb�uhj  hja  ubh)��}�(h�F <http://cgit.freedesktop.org/systemd/systemd/tree/hwdb/70-mouse.hwdb>�h]�h}�(h]��	udev-hwdb�ah]�h]��	udev hwdb�ah]�h]��refuri�j�  uhh�
referenced�Khja  ubh9�.
The �����}�(h�.
The �hja  hhhNhNubh�)��}�(h�``mouse-dpi-tool``�h]�h9�mouse-dpi-tool�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh�hja  ubh9� utility provided by
�����}�(h� utility provided by
�hja  hhhNhNubj�  )��}�(h�=`libevdev <https://freedesktop.org/wiki/Software/libevdev/>`_�h]�h9�libevdev�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]��name��libevdev�j�  �/https://freedesktop.org/wiki/Software/libevdev/�uhj  hja  ubh)��}�(h�2 <https://freedesktop.org/wiki/Software/libevdev/>�h]�h}�(h]��libevdev�ah]�h]��libevdev�ah]�h]��refuri�j�  uhhj�  Khja  ubh9�3 should be
used to measure a device’s resolution.�����}�(h�1 should be
used to measure a device's resolution.�hja  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK>hjP  hhubhE)��}�(h�<The format of the property for single-resolution mice is: ::�h]�h9�9The format of the property for single-resolution mice is:�����}�(h�9The format of the property for single-resolution mice is:�hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKFhjP  hhubh �literal_block���)��}�(h�MOUSE_DPI=resolution@frequency�h]�h9�MOUSE_DPI=resolution@frequency�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]��	xml:space��preserve�uhj�  hKMhjP  hhhh,ubhE)��}�(h��The resolution is in dots per inch, the frequency in Hz.
The format of the property for multi-resolution mice may list multiple
resolutions and frequencies: ::�h]�h9��The resolution is in dots per inch, the frequency in Hz.
The format of the property for multi-resolution mice may list multiple
resolutions and frequencies:�����}�(h��The resolution is in dots per inch, the frequency in Hz.
The format of the property for multi-resolution mice may list multiple
resolutions and frequencies:�hj  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKJhjP  hhubj�  )��}�(h�MOUSE_DPI=r1@f1 *r2@f2 r3@f3�h]�h9�MOUSE_DPI=r1@f1 *r2@f2 r3@f3�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]�j  j  uhj�  hKShjP  hhhh,ubhE)��}�(h�9The default frequency must be pre-fixed with an asterisk.�h]�h9�9The default frequency must be pre-fixed with an asterisk.�����}�(hj"  hj   hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKPhjP  hhubhE)��}�(h�/For example, these two properties are valid: ::�h]�h9�,For example, these two properties are valid:�����}�(h�,For example, these two properties are valid:�hj.  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKRhjP  hhubj�  )��}�(h�>MOUSE_DPI=800@125
MOUSE_DPI=400@125 800@125 *1000@500 5500@500�h]�h9�>MOUSE_DPI=800@125
MOUSE_DPI=400@125 800@125 *1000@500 5500@500�����}�(hhhj=  ubah}�(h]�h]�h]�h]�h]�j  j  uhj�  hKYhjP  hhhh,ubhE)��}�(h�yThe behavior for a malformed property is undefined. If the property is
unset, libinput assumes the resolution is 1000dpi.�h]�h9�yThe behavior for a malformed property is undefined. If the property is
unset, libinput assumes the resolution is 1000dpi.�����}�(hjM  hjK  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKWhjP  hhubhE)��}�(h��Note that HW does not usually provide information about run-time
resolution changes, libinput will thus not detect when a resolution
changes to the non-default value.�h]�h9��Note that HW does not usually provide information about run-time
resolution changes, libinput will thus not detect when a resolution
changes to the non-default value.�����}�(hj[  hjY  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKZhjP  hhubeh}�(h]�(�setting-custom-dpi-settings�jD  eh]�h]�(�setting custom dpi settings��"motion_normalization_customization�eh]�h]�uhh-hh/hhhh,hK<h�}�jm  j:  sh�}�jD  j:  subeh}�(h]�(� normalization-of-relative-motion�h+eh]�h]�(� normalization of relative motion��motion_normalization�eh]�h]�uhh-hhhhhh,hKh�}�jx  h sh�}�h+h subeh}�(h]�h]�h]�h]�h]��source�h,uhh�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(h2N�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}�(�git_version�h �substitution_definition���)��}�(h�,.. |git_version| replace:: :commit:`3325b81`�h]�j�  )��}�(h�git commit 3325b81�h]�h9�git commit 3325b81�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]��internal���refuri��?https://gitlab.freedesktop.org/libinput/libinput/commit/3325b81�uhj  hj�  ubah}�(h]�h]�h]�j�  ah]�h]�uhj�  h�<rst_prolog>�hKhhub�git_version_full�j�  )��}�(h�].. |git_version_full| replace:: :commit:`<function get_git_version_full at 0x7f4db2896f28>`

�h]�j�  )��}�(h�<git commit <function get_git_version_full at 0x7f4db2896f28>�h]�h9�<git commit <function get_git_version_full at 0x7f4db2896f28>�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]��internal���refuri��ihttps://gitlab.freedesktop.org/libinput/libinput/commit/<function get_git_version_full at 0x7f4db2896f28>�uhj  hj�  ubah}�(h]�h]�h]�j�  ah]�h]�uhj�  hj�  hKhhubu�substitution_names�}�(�git_version�j�  �git_version_full�j�  u�refnames�}��refids�}�(h+]�h ah�]�h�ah�]�h�ajD  ]�j:  au�nameids�}�(jx  h+jw  jt  h�h�h�h�jK  h�jJ  jG  jm  jD  jl  ji  j�  j�  j�  j�  u�	nametypes�}�(jx  �jw  Nh�h�NjK  �jJ  Njm  �jl  Nj�  �j�  �uh}�(h+h/jt  h/h�h�h�h�h�h�jG  h�jD  jP  ji  jP  j�  j�  j�  j�  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]��transform_messages�]�(h �system_message���)��}�(hhh]�hE)��}�(hhh]�h9�:Hyperlink target "motion-normalization" is not referenced.�����}�(hhhj?  ubah}�(h]�h]�h]�h]�h]�uhhDhj<  ubah}�(h]�h]�h]�h]�h]��level�K�type��INFO��source�h,�line�Kuhj:  ubj;  )��}�(hhh]�hE)��}�(hhh]�h9�CHyperlink target "motion-normalization-touchpad" is not referenced.�����}�(hhhjZ  ubah}�(h]�h]�h]�h]�h]�uhhDhjW  ubah}�(h]�h]�h]�h]�h]��level�K�type�jT  �source�h,�line�K'uhj:  ubj;  )��}�(hhh]�hE)��}�(hhh]�h9�AHyperlink target "motion-normalization-tablet" is not referenced.�����}�(hhhjt  ubah}�(h]�h]�h]�h]�h]�uhhDhjq  ubah}�(h]�h]�h]�h]�h]��level�K�type�jT  �source�h,�line�K5uhj:  ubj;  )��}�(hhh]�hE)��}�(hhh]�h9�HHyperlink target "motion-normalization-customization" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�jT  �source�h,�line�K=uhj:  ube�transformer�N�
decoration�Nhhub.