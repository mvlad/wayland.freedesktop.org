���u      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �block_quote���)��}�(hhh]��
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�h	�parent�hhh�source�N�line�Nubh �target���)��}�(h�.. _test-suite:�h]�h}�(h]�h]�h]�h]�h]��refid��
test-suite�uhhhKhhhhh�6/home/whot/code/libinput/build/doc/user/test-suite.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�libinput test suite�h]�h �Text����libinput test suite�����}�(hh6hh4hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hh/hhhh,hKubh �	paragraph���)��}�(hX]  libinput ships with a number of tests all run automatically on ``ninja test``.
The primary test suite is the ``libinput-test-suite``. When testing,
the ``libinput-test-suite`` should always be invoked to check for
behavior changes. The test suite relies on the kernel and udev to function
correctly. It is not suitable for running inside containers.�h]�(h9�?libinput ships with a number of tests all run automatically on �����}�(h�?libinput ships with a number of tests all run automatically on �hhFhhhNhNubh �literal���)��}�(h�``ninja test``�h]�h9�
ninja test�����}�(hhhhQubah}�(h]�h]�h]�h]�h]�uhhOhhFubh9� .
The primary test suite is the �����}�(h� .
The primary test suite is the �hhFhhhNhNubhP)��}�(h�``libinput-test-suite``�h]�h9�libinput-test-suite�����}�(hhhhdubah}�(h]�h]�h]�h]�h]�uhhOhhFubh9�. When testing,
the �����}�(h�. When testing,
the �hhFhhhNhNubhP)��}�(h�``libinput-test-suite``�h]�h9�libinput-test-suite�����}�(hhhhwubah}�(h]�h]�h]�h]�h]�uhhOhhFubh9�� should always be invoked to check for
behavior changes. The test suite relies on the kernel and udev to function
correctly. It is not suitable for running inside containers.�����}�(h�� should always be invoked to check for
behavior changes. The test suite relies on the kernel and udev to function
correctly. It is not suitable for running inside containers.�hhFhhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKhh/hhubh �note���)��}�(h�```ninja test`` runs more than just the test suite, you **must**
run all tests for full coverage.�h]�hE)��}�(h�```ninja test`` runs more than just the test suite, you **must**
run all tests for full coverage.�h]�(hP)��}�(h�``ninja test``�h]�h9�
ninja test�����}�(hhhh�ubah}�(h]�h]�h]�h]�h]�uhhOhh�ubh9�) runs more than just the test suite, you �����}�(h�) runs more than just the test suite, you �hh�ubh �strong���)��}�(h�**must**�h]�h9�must�����}�(hhhh�ubah}�(h]�h]�h]�h]�h]�uhh�hh�ubh9�!
run all tests for full coverage.�����}�(h�!
run all tests for full coverage.�hh�ubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKhh�ubah}�(h]�h]�h]�h]�h]�uhh�hh/hhhh,hNubhE)��}�(h��The test suite runner uses
`Check <http://check.sourceforge.net/doc/check_html/>`_ underneath the hood
but most of the functionality is abstracted into *litest* wrappers.�h]�(h9�The test suite runner uses
�����}�(h�The test suite runner uses
�hh�hhhNhNubh �	reference���)��}�(h�7`Check <http://check.sourceforge.net/doc/check_html/>`_�h]�h9�Check�����}�(hhhh�ubah}�(h]�h]�h]�h]�h]��name��Check��refuri��,http://check.sourceforge.net/doc/check_html/�uhh�hh�ubh)��}�(h�/ <http://check.sourceforge.net/doc/check_html/>�h]�h}�(h]��check�ah]�h]��check�ah]�h]��refuri�h�uhh�
referenced�Khh�ubh9�F underneath the hood
but most of the functionality is abstracted into �����}�(h�F underneath the hood
but most of the functionality is abstracted into �hh�hhhNhNubh �emphasis���)��}�(h�*litest*�h]�h9�litest�����}�(hhhj   ubah}�(h]�h]�h]�h]�h]�uhh�hh�ubh9�
 wrappers.�����}�(h�
 wrappers.�hh�hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKhh/hhubhE)��}�(hX�  The test suite runner has a make-like job control enabled by the ``-j`` or
``--jobs`` flag and will fork off as many parallel processes as given by this
flag. The default if unspecified is 8. When debugging a specific test case
failure it is recommended to employ test filtures (see :ref:`test-filtering`)
and disable parallel tests. The test suite automatically disables parallel
make when run in gdb.�h]�(h9�AThe test suite runner has a make-like job control enabled by the �����}�(h�AThe test suite runner has a make-like job control enabled by the �hj  hhhNhNubhP)��}�(h�``-j``�h]�h9�-j�����}�(hhhj"  ubah}�(h]�h]�h]�h]�h]�uhhOhj  ubh9� or
�����}�(h� or
�hj  hhhNhNubhP)��}�(h�
``--jobs``�h]�h9�--jobs�����}�(hhhj5  ubah}�(h]�h]�h]�h]�h]�uhhOhj  ubh9�� flag and will fork off as many parallel processes as given by this
flag. The default if unspecified is 8. When debugging a specific test case
failure it is recommended to employ test filtures (see �����}�(h�� flag and will fork off as many parallel processes as given by this
flag. The default if unspecified is 8. When debugging a specific test case
failure it is recommended to employ test filtures (see �hj  hhhNhNub�sphinx.addnodes��pending_xref���)��}�(h�:ref:`test-filtering`�h]�h �inline���)��}�(hjM  h]�h9�test-filtering�����}�(hhhjQ  ubah}�(h]�h]�(�xref��std��std-ref�eh]�h]�h]�uhjO  hjK  ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�j\  �refexplicit���	reftarget��test-filtering��refdoc��
test-suite��refwarn��uhjI  hh,hKhj  ubh9�b)
and disable parallel tests. The test suite automatically disables parallel
make when run in gdb.�����}�(h�b)
and disable parallel tests. The test suite automatically disables parallel
make when run in gdb.�hj  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKhh/hhubh)��}�(h�.. _test-config:�h]�h}�(h]�h]�h]�h]�h]�h*�test-config�uhhhK hh/hhhh,ubh.)��}�(hhh]�(h3)��}�(h�"X.Org config to avoid interference�h]�h9�"X.Org config to avoid interference�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj�  hhhh,hKubhE)��}�(h��uinput devices created by the test suite are usually recognised by X as
input devices. All events sent through these devices will generate X events
and interfere with your desktop.�h]�h9��uinput devices created by the test suite are usually recognised by X as
input devices. All events sent through these devices will generate X events
and interfere with your desktop.�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK!hj�  hhubhE)��}�(h��Copy the file ``$srcdir/test/50-litest.conf`` into your ``/etc/X11/xorg.conf.d``
and restart X. This will ignore any litest devices and thus not interfere
with your desktop.�h]�(h9�Copy the file �����}�(h�Copy the file �hj�  hhhNhNubhP)��}�(h�``$srcdir/test/50-litest.conf``�h]�h9�$srcdir/test/50-litest.conf�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhOhj�  ubh9� into your �����}�(h� into your �hj�  hhhNhNubhP)��}�(h�``/etc/X11/xorg.conf.d``�h]�h9�/etc/X11/xorg.conf.d�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhOhj�  ubh9�]
and restart X. This will ignore any litest devices and thus not interfere
with your desktop.�����}�(h�]
and restart X. This will ignore any litest devices and thus not interfere
with your desktop.�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK%hj�  hhubh)��}�(h�.. _test-root:�h]�h}�(h]�h]�h]�h]�h]�h*�	test-root�uhhhK.hj�  hhhh,ubeh}�(h]�(�"x-org-config-to-avoid-interference�j�  eh]�h]�(�"x.org config to avoid interference��test-config�eh]�h]�uhh-hh/hhhh,hK�expect_referenced_by_name�}�j�  j{  s�expect_referenced_by_id�}�j�  j{  subh.)��}�(hhh]�(h3)��}�(h�!Permissions required to run tests�h]�h9�!Permissions required to run tests�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj�  hhhh,hK-ubhE)��}�(hX1  Most tests require the creation of uinput devices and access to the
resulting ``/dev/input/eventX`` nodes. Some tests require temporary udev rules.
**This usually requires the tests to be run as root**. If not run as
root, the test suite runner will exit with status 77, interpreted as
"skipped" by ninja.�h]�(h9�NMost tests require the creation of uinput devices and access to the
resulting �����}�(h�NMost tests require the creation of uinput devices and access to the
resulting �hj  hhhNhNubhP)��}�(h�``/dev/input/eventX``�h]�h9�/dev/input/eventX�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]�uhhOhj  ubh9�1 nodes. Some tests require temporary udev rules.
�����}�(h�1 nodes. Some tests require temporary udev rules.
�hj  hhhNhNubh�)��}�(h�5**This usually requires the tests to be run as root**�h]�h9�1This usually requires the tests to be run as root�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]�uhh�hj  ubh9�l. If not run as
root, the test suite runner will exit with status 77, interpreted as
“skipped” by ninja.�����}�(h�h. If not run as
root, the test suite runner will exit with status 77, interpreted as
"skipped" by ninja.�hj  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK/hj�  hhubh)��}�(h�.. _test-filtering:�h]�h}�(h]�h]�h]�h]�h]�h*�test-filtering�uhhhK:hj�  hhhh,ubeh}�(h]�(�!permissions-required-to-run-tests�j�  eh]�h]�(�!permissions required to run tests��	test-root�eh]�h]�uhh-hh/hhhh,hK-j�  }�jI  j�  sj�  }�j�  j�  subh.)��}�(hhh]�(h3)��}�(h�Selective running of tests�h]�h9�Selective running of tests�����}�(hjS  hjQ  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hjN  hhhh,hK9ubhE)��}�(hX�  litest's tests are grouped into test groups, test names and devices. A test
group is e.g.  "touchpad:tap" and incorporates all tapping-related tests for
touchpads. Each test function is (usually) run with one or more specific
devices. The ``--list`` commandline argument shows the list of suites and
tests. This is useful when trying to figure out if a specific test is
run for a device.�h]�(h9��litest’s tests are grouped into test groups, test names and devices. A test
group is e.g.  “touchpad:tap” and incorporates all tapping-related tests for
touchpads. Each test function is (usually) run with one or more specific
devices. The �����}�(h��litest's tests are grouped into test groups, test names and devices. A test
group is e.g.  "touchpad:tap" and incorporates all tapping-related tests for
touchpads. Each test function is (usually) run with one or more specific
devices. The �hj_  hhhNhNubhP)��}�(h�
``--list``�h]�h9�--list�����}�(hhhjh  ubah}�(h]�h]�h]�h]�h]�uhhOhj_  ubh9�� commandline argument shows the list of suites and
tests. This is useful when trying to figure out if a specific test is
run for a device.�����}�(h�� commandline argument shows the list of suites and
tests. This is useful when trying to figure out if a specific test is
run for a device.�hj_  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK;hjN  hhubh �literal_block���)��}�(hX�  $ ./builddir/libinput-test-suite --list
...
pointer:left-handed:
   pointer_left_handed_during_click_multiple_buttons:
           trackpoint
           ms-surface-cover
           mouse-wheelclickcount
           mouse-wheelclickangle
           low-dpi-mouse
           mouse-roccat
           mouse-wheel-tilt
           mouse
           logitech-trackball
           cyborg-rat
           magicmouse
   pointer_left_handed_during_click:
           trackpoint
           ms-surface-cover
           mouse-wheelclickcount
           mouse-wheelclickangle
           low-dpi-mouse
           mouse-roccat
           mouse-wheel-tilt
           mouse
           logitech-trackball
           cyborg-rat
           litest-magicmouse-device
   pointer_left_handed:
           trackpoint
           ms-surface-cover
           mouse-wheelclickcount
           mouse-wheelclickangle
           low-dpi-mouse
           mouse-roccat
           mouse-wheel-tilt
           mouse
...�h]�h9X�  $ ./builddir/libinput-test-suite --list
...
pointer:left-handed:
   pointer_left_handed_during_click_multiple_buttons:
           trackpoint
           ms-surface-cover
           mouse-wheelclickcount
           mouse-wheelclickangle
           low-dpi-mouse
           mouse-roccat
           mouse-wheel-tilt
           mouse
           logitech-trackball
           cyborg-rat
           magicmouse
   pointer_left_handed_during_click:
           trackpoint
           ms-surface-cover
           mouse-wheelclickcount
           mouse-wheelclickangle
           low-dpi-mouse
           mouse-roccat
           mouse-wheel-tilt
           mouse
           logitech-trackball
           cyborg-rat
           litest-magicmouse-device
   pointer_left_handed:
           trackpoint
           ms-surface-cover
           mouse-wheelclickcount
           mouse-wheelclickangle
           low-dpi-mouse
           mouse-roccat
           mouse-wheel-tilt
           mouse
...�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]��	xml:space��preserve�uhj�  hKJhjN  hhhh,ubhE)��}�(hX,  In the above example, the "pointer:left-handed" suite contains multiple
tests, e.g. "pointer_left_handed_during_click" (this is also the function
name of the test, making it easy to grep for). This particular test is run
for various devices including the trackpoint device and the magic mouse
device.�h]�h9X4  In the above example, the “pointer:left-handed” suite contains multiple
tests, e.g. “pointer_left_handed_during_click” (this is also the function
name of the test, making it easy to grep for). This particular test is run
for various devices including the trackpoint device and the magic mouse
device.�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKlhjN  hhubhE)��}�(h��The "no device" entry signals that litest does not instantiate a uinput
device for a specific test (though the test itself may
instantiate one).�h]�h9��The “no device” entry signals that litest does not instantiate a uinput
device for a specific test (though the test itself may
instantiate one).�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKrhjN  hhubhE)��}�(h��The ``--filter-test`` argument enables selective running of tests through
basic shell-style function name matching. For example:�h]�(h9�The �����}�(h�The �hj�  hhhNhNubhP)��}�(h�``--filter-test``�h]�h9�--filter-test�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhOhj�  ubh9�k argument enables selective running of tests through
basic shell-style function name matching. For example:�����}�(h�k argument enables selective running of tests through
basic shell-style function name matching. For example:�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKvhjN  hhubj�  )��}�(h�:$ ./builddir/libinput-test-suite --filter-test="*1fg_tap*"�h]�h9�:$ ./builddir/libinput-test-suite --filter-test="*1fg_tap*"�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�j�  j�  uhj�  hK�hjN  hhhh,ubhE)��}�(h��The ``--filter-device`` argument enables selective running of tests through
basic shell-style device name matching. The device names matched are the
litest-specific shortnames, see the output of ``--list``. For example:�h]�(h9�The �����}�(h�The �hj�  hhhNhNubhP)��}�(h�``--filter-device``�h]�h9�--filter-device�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhOhj�  ubh9�� argument enables selective running of tests through
basic shell-style device name matching. The device names matched are the
litest-specific shortnames, see the output of �����}�(h�� argument enables selective running of tests through
basic shell-style device name matching. The device names matched are the
litest-specific shortnames, see the output of �hj�  hhhNhNubhP)��}�(h�
``--list``�h]�h9�--list�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhOhj�  ubh9�. For example:�����}�(h�. For example:�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKhjN  hhubj�  )��}�(h�=$ ./builddir/libinput-test-suite --filter-device="synaptics*"�h]�h9�=$ ./builddir/libinput-test-suite --filter-device="synaptics*"�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]�j�  j�  uhj�  hK�hjN  hhhh,ubhE)��}�(h��The ``--filter-group`` argument enables selective running of test groups
through basic shell-style test group matching. The test groups matched are
litest-specific test groups, see the output of ``--list``. For example:�h]�(h9�The �����}�(h�The �hj"  hhhNhNubhP)��}�(h�``--filter-group``�h]�h9�--filter-group�����}�(hhhj+  ubah}�(h]�h]�h]�h]�h]�uhhOhj"  ubh9�� argument enables selective running of test groups
through basic shell-style test group matching. The test groups matched are
litest-specific test groups, see the output of �����}�(h�� argument enables selective running of test groups
through basic shell-style test group matching. The test groups matched are
litest-specific test groups, see the output of �hj"  hhhNhNubhP)��}�(h�
``--list``�h]�h9�--list�����}�(hhhj>  ubah}�(h]�h]�h]�h]�h]�uhhOhj"  ubh9�. For example:�����}�(h�. For example:�hj"  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hjN  hhubj�  )��}�(h�B$ ./builddir/libinput-test-suite --filter-group="touchpad:*hover*"�h]�h9�B$ ./builddir/libinput-test-suite --filter-group="touchpad:*hover*"�����}�(hhhjW  ubah}�(h]�h]�h]�h]�h]�j�  j�  uhj�  hK�hjN  hhhh,ubhE)��}�(h��The ``--filter-device`` and ``--filter-group`` arguments can be combined with
``--list`` to show which groups and devices will be affected.�h]�(h9�The �����}�(h�The �hje  hhhNhNubhP)��}�(h�``--filter-device``�h]�h9�--filter-device�����}�(hhhjn  ubah}�(h]�h]�h]�h]�h]�uhhOhje  ubh9� and �����}�(h� and �hje  hhhNhNubhP)��}�(h�``--filter-group``�h]�h9�--filter-group�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhOhje  ubh9�  arguments can be combined with
�����}�(h�  arguments can be combined with
�hje  hhhNhNubhP)��}�(h�
``--list``�h]�h9�--list�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhOhje  ubh9�3 to show which groups and devices will be affected.�����}�(h�3 to show which groups and devices will be affected.�hje  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hjN  hhubh)��}�(h�.. _test-verbosity:�h]�h}�(h]�h]�h]�h]�h]�h*�test-verbosity�uhhhK�hjN  hhhh,ubeh}�(h]�(�selective-running-of-tests�jB  eh]�h]�(�selective running of tests��test-filtering�eh]�h]�uhh-hh/hhhh,hK9j�  }�j�  j8  sj�  }�jB  j8  subh.)��}�(hhh]�(h3)��}�(h�Controlling test output�h]�h9�Controlling test output�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj�  hhhh,hK�ubhE)��}�(h��Each test supports the ``--verbose`` commandline option to enable debugging
output, see **libinput_log_set_priority()** for details. The ``LITEST_VERBOSE``
environment variable, if set, also enables verbose mode.�h]�(h9�Each test supports the �����}�(h�Each test supports the �hj�  hhhNhNubhP)��}�(h�``--verbose``�h]�h9�	--verbose�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhOhj�  ubh9�4 commandline option to enable debugging
output, see �����}�(h�4 commandline option to enable debugging
output, see �hj�  hhhNhNubh�)��}�(h�**libinput_log_set_priority()**�h]�h9�libinput_log_set_priority()�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh9� for details. The �����}�(h� for details. The �hj�  hhhNhNubhP)��}�(h�``LITEST_VERBOSE``�h]�h9�LITEST_VERBOSE�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]�uhhOhj�  ubh9�9
environment variable, if set, also enables verbose mode.�����}�(h�9
environment variable, if set, also enables verbose mode.�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj�  hhubj�  )��}�(h�H$ ./builddir/libinput-test-suite --verbose
$ LITEST_VERBOSE=1 ninja test�h]�h9�H$ ./builddir/libinput-test-suite --verbose
$ LITEST_VERBOSE=1 ninja test�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]�j�  j�  uhj�  hK�hj�  hhhh,ubh)��}�(h�.. _test-installed:�h]�h}�(h]�h]�h]�h]�h]�h*�test-installed�uhhhK�hj�  hhhh,ubeh}�(h]�(�controlling-test-output�j�  eh]�h]�(�controlling test output��test-verbosity�eh]�h]�uhh-hh/hhhh,hK�j�  }�j;  j�  sj�  }�j�  j�  subh.)��}�(hhh]�(h3)��}�(h�Installing the test suite�h]�h9�Installing the test suite�����}�(hjE  hjC  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj@  hhhh,hK�ubhE)��}�(h��If libinput is configured to install the tests, the test suite is available
as the ``libinput test-suite`` command. When run as installed binary, the
behavior of the test suite changes:�h]�(h9�SIf libinput is configured to install the tests, the test suite is available
as the �����}�(h�SIf libinput is configured to install the tests, the test suite is available
as the �hjQ  hhhNhNubhP)��}�(h�``libinput test-suite``�h]�h9�libinput test-suite�����}�(hhhjZ  ubah}�(h]�h]�h]�h]�h]�uhhOhjQ  ubh9�O command. When run as installed binary, the
behavior of the test suite changes:�����}�(h�O command. When run as installed binary, the
behavior of the test suite changes:�hjQ  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj@  hhubh �bullet_list���)��}�(hhh]�(h �	list_item���)��}�(h�?the ``libinput.so`` used is the one in the library lookup paths�h]�hE)��}�(hj|  h]�(h9�the �����}�(h�the �hj~  ubhP)��}�(h�``libinput.so``�h]�h9�libinput.so�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhOhj~  ubh9�, used is the one in the library lookup paths�����}�(h�, used is the one in the library lookup paths�hj~  ubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hjz  ubah}�(h]�h]�h]�h]�h]�uhjx  hju  hhhh,hNubjy  )��}�(h�^no system-wide quirks are installed by the test suite, only those specific
to the test devices�h]�hE)��}�(h�^no system-wide quirks are installed by the test suite, only those specific
to the test devices�h]�h9�^no system-wide quirks are installed by the test suite, only those specific
to the test devices�����}�(hj�  hj�  ubah}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj�  ubah}�(h]�h]�h]�h]�h]�uhjx  hju  hhhh,hNubjy  )��}�(h�qtest device-specific quirks are installed in the system-wide quirks
directory, usually ``/usr/share/libinput/``.
�h]�hE)��}�(h�ptest device-specific quirks are installed in the system-wide quirks
directory, usually ``/usr/share/libinput/``.�h]�(h9�Wtest device-specific quirks are installed in the system-wide quirks
directory, usually �����}�(h�Wtest device-specific quirks are installed in the system-wide quirks
directory, usually �hj�  ubhP)��}�(h�``/usr/share/libinput/``�h]�h9�/usr/share/libinput/�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhOhj�  ubh9�.�����}�(h�.�hj�  ubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj�  ubah}�(h]�h]�h]�h]�h]�uhjx  hju  hhhh,hNubeh}�(h]�h]�h]�h]�h]��bullet��-�uhjs  hh,hK�hj@  hhubhE)��}�(h��It is not advisable to run ``libinput test-suite`` on a production machine.
Data loss may occur. The primary use-case for the installed test suite is
verification of distribution composes.�h]�(h9�It is not advisable to run �����}�(h�It is not advisable to run �hj�  hhhNhNubhP)��}�(h�``libinput test-suite``�h]�h9�libinput test-suite�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhOhj�  ubh9�� on a production machine.
Data loss may occur. The primary use-case for the installed test suite is
verification of distribution composes.�����}�(h�� on a production machine.
Data loss may occur. The primary use-case for the installed test suite is
verification of distribution composes.�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj@  hhubh�)��}�(h��The ``prefix`` is still used by the test suite. For verification
of a system package, the test suite must be configured with the same prefix.�h]�hE)��}�(h��The ``prefix`` is still used by the test suite. For verification
of a system package, the test suite must be configured with the same prefix.�h]�(h9�The �����}�(h�The �hj  ubhP)��}�(h�
``prefix``�h]�h9�prefix�����}�(hhhj   ubah}�(h]�h]�h]�h]�h]�uhhOhj  ubh9� is still used by the test suite. For verification
of a system package, the test suite must be configured with the same prefix.�����}�(h� is still used by the test suite. For verification
of a system package, the test suite must be configured with the same prefix.�hj  ubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj  ubah}�(h]�h]�h]�h]�h]�uhh�hj@  hhhh,hNubhE)��}�(h�[To configure libinput to install the tests, use the ``-Dinstall-tests=true``
meson option::�h]�(h9�4To configure libinput to install the tests, use the �����}�(h�4To configure libinput to install the tests, use the �hj?  hhhNhNubhP)��}�(h�``-Dinstall-tests=true``�h]�h9�-Dinstall-tests=true�����}�(hhhjH  ubah}�(h]�h]�h]�h]�h]�uhhOhj?  ubh9�
meson option:�����}�(h�
meson option:�hj?  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj@  hhubj�  )��}�(h�B$ meson builddir -Dtests=true -Dinstall-tests=true <other options>�h]�h9�B$ meson builddir -Dtests=true -Dinstall-tests=true <other options>�����}�(hhhja  ubah}�(h]�h]�h]�h]�h]�j�  j�  uhj�  hK�hj@  hhhh,ubeh}�(h]�(�installing-the-test-suite�j4  eh]�h]�(�installing the test suite��test-installed�eh]�h]�uhh-hh/hhhh,hK�j�  }�ju  j*  sj�  }�j4  j*  subeh}�(h]�(�libinput-test-suite�h+eh]�h]�(�libinput test suite��
test-suite�eh]�h]�uhh-hhhhhh,hKj�  }�j�  h sj�  }�h+h subeh}�(h]�h]�h]�h]�h]��source�h,uhh�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(h2N�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}�(�git_version�h �substitution_definition���)��}�(h�,.. |git_version| replace:: :commit:`3325b81`�h]�h�)��}�(h�git commit 3325b81�h]�h9�git commit 3325b81�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]��internal���refuri��?https://gitlab.freedesktop.org/libinput/libinput/commit/3325b81�uhh�hj�  ubah}�(h]�h]�h]�j�  ah]�h]�uhj�  h�<rst_prolog>�hKhhub�git_version_full�j�  )��}�(h�].. |git_version_full| replace:: :commit:`<function get_git_version_full at 0x7f4db2896f28>`

�h]�h�)��}�(h�<git commit <function get_git_version_full at 0x7f4db2896f28>�h]�h9�<git commit <function get_git_version_full at 0x7f4db2896f28>�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]��internal���refuri��ihttps://gitlab.freedesktop.org/libinput/libinput/commit/<function get_git_version_full at 0x7f4db2896f28>�uhh�hj�  ubah}�(h]�h]�h]�j�  ah]�h]�uhj�  hj�  hKhhubu�substitution_names�}�(�git_version�j�  �git_version_full�j�  u�refnames�}��refids�}�(h+]�h aj�  ]�j{  aj�  ]�j�  ajB  ]�j8  aj�  ]�j�  aj4  ]�j*  au�nameids�}�(j�  h+j  j|  h�h�j�  j�  j�  j�  jI  j�  jH  jE  j�  jB  j�  j�  j;  j�  j:  j7  ju  j4  jt  jq  u�	nametypes�}�(j�  �j  Nh�j�  �j�  NjI  �jH  Nj�  �j�  Nj;  �j:  Nju  �jt  Nuh}�(h+h/j|  h/h�h�j�  j�  j�  j�  j�  j�  jE  j�  jB  jN  j�  jN  j�  j�  j7  j�  j4  j@  jq  j@  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]��transform_messages�]�(h �system_message���)��}�(hhh]�hE)��}�(hhh]�h9�0Hyperlink target "test-suite" is not referenced.�����}�(hhhjI  ubah}�(h]�h]�h]�h]�h]�uhhDhjF  ubah}�(h]�h]�h]�h]�h]��level�K�type��INFO��source�h,�line�KuhjD  ubjE  )��}�(hhh]�hE)��}�(hhh]�h9�1Hyperlink target "test-config" is not referenced.�����}�(hhhjd  ubah}�(h]�h]�h]�h]�h]�uhhDhja  ubah}�(h]�h]�h]�h]�h]��level�K�type�j^  �source�h,�line�K uhjD  ubjE  )��}�(hhh]�hE)��}�(hhh]�h9�/Hyperlink target "test-root" is not referenced.�����}�(hhhj~  ubah}�(h]�h]�h]�h]�h]�uhhDhj{  ubah}�(h]�h]�h]�h]�h]��level�K�type�j^  �source�h,�line�K.uhjD  ubjE  )��}�(hhh]�hE)��}�(hhh]�h9�4Hyperlink target "test-filtering" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j^  �source�h,�line�K:uhjD  ubjE  )��}�(hhh]�hE)��}�(hhh]�h9�4Hyperlink target "test-verbosity" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j^  �source�h,�line�K�uhjD  ubjE  )��}�(hhh]�hE)��}�(hhh]�h9�4Hyperlink target "test-installed" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j^  �source�h,�line�K�uhjD  ube�transformer�N�
decoration�Nhhub.