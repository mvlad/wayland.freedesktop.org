��p6      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �block_quote���)��}�(hhh]��
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�h	�parent�hhh�source�N�line�Nubh �target���)��}�(h�.. _trackpoints:�h]�h}�(h]�h]�h]�h]�h]��refid��trackpoints�uhhhKhhhhh�7/home/whot/code/libinput/build/doc/user/trackpoints.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�Trackpoints and Pointing Sticks�h]�h �Text����Trackpoints and Pointing Sticks�����}�(hh6hh4hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hh/hhhh,hKubh �	paragraph���)��}�(h��This page provides an overview of trackpoint handling in libinput, also
refered to as Pointing Stick or Trackstick. The device itself is usually a
round plastic stick between the G, H and B keys with a set of buttons below
the space bar.�h]�h9��This page provides an overview of trackpoint handling in libinput, also
refered to as Pointing Stick or Trackstick. The device itself is usually a
round plastic stick between the G, H and B keys with a set of buttons below
the space bar.�����}�(hhHhhFhhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKhh/hhubh �figure���)��}�(hhh]�(h �image���)��}�(h�F.. figure:: button-scrolling.svg
    :align: center

    A trackpoint
�h]�h}�(h]�h]�h]�h]�h]��uri��button-scrolling.svg��
candidates�}��*�hfsuhhYhhVhh,hKubh �caption���)��}�(h�A trackpoint�h]�h9�A trackpoint�����}�(hhnhhlubah}�(h]�h]�h]�h]�h]�uhhjhh,hKhhVubeh}�(h]��id1�ah]�h]�h]�h]��align��center�uhhThKhh/hhhh,ubhE)��}�(h��libinput always treats the buttons below the space bar as the buttons that
belong to the trackpoint even on the few laptops where the buttons are not
physically wired to the trackpoint device anyway, see :ref:`t440_support`.�h]�(h9��libinput always treats the buttons below the space bar as the buttons that
belong to the trackpoint even on the few laptops where the buttons are not
physically wired to the trackpoint device anyway, see �����}�(h��libinput always treats the buttons below the space bar as the buttons that
belong to the trackpoint even on the few laptops where the buttons are not
physically wired to the trackpoint device anyway, see �hh�hhhNhNub�sphinx.addnodes��pending_xref���)��}�(h�:ref:`t440_support`�h]�h �inline���)��}�(hh�h]�h9�t440_support�����}�(hhhh�ubah}�(h]�h]�(�xref��std��std-ref�eh]�h]�h]�uhh�hh�ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�h��refexplicit���	reftarget��t440_support��refdoc��trackpoints��refwarn��uhh�hh,hKhh�ubh9�.�����}�(h�.�hh�hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKhh/hhubh)��}�(h�.. _trackpoint_buttonscroll:�h]�h}�(h]�h]�h]�h]�h]�h*�trackpoint-buttonscroll�uhhhKhh/hhhh,ubh.)��}�(hhh]�(h3)��}�(h�Button scrolling on trackpoints�h]�h9�Button scrolling on trackpoints�����}�(hh�hh�hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hh�hhhh,hKubhE)��}�(h��Trackpoint devices have :ref:`button_scrolling` enabled by default. This may
interfer with middle-button dragging, if middle-button dragging is required
by a user then button scrolling must be disabled.�h]�(h9�Trackpoint devices have �����}�(h�Trackpoint devices have �hh�hhhNhNubh�)��}�(h�:ref:`button_scrolling`�h]�h�)��}�(hh�h]�h9�button_scrolling�����}�(hhhh�ubah}�(h]�h]�(h��std��std-ref�eh]�h]�h]�uhh�hh�ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�h�refexplicit��h��button_scrolling�h�h�h��uhh�hh,hKhh�ubh9�� enabled by default. This may
interfer with middle-button dragging, if middle-button dragging is required
by a user then button scrolling must be disabled.�����}�(h�� enabled by default. This may
interfer with middle-button dragging, if middle-button dragging is required
by a user then button scrolling must be disabled.�hh�hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKhh�hhubh)��}�(h�.. _trackpoint_range:�h]�h}�(h]�h]�h]�h]�h]�h*�trackpoint-range�uhhhK$hh�hhhh,ubeh}�(h]�(�button-scrolling-on-trackpoints�h�eh]�h]�(�button scrolling on trackpoints��trackpoint_buttonscroll�eh]�h]�uhh-hh/hhhh,hK�expect_referenced_by_name�}�j  h�s�expect_referenced_by_id�}�h�h�subh.)��}�(hhh]�(h3)��}�(h�Motion range on trackpoints�h]�h9�Motion range on trackpoints�����}�(hj*  hj(  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj%  hhhh,hK#ubhE)��}�(hX"  It is difficult to associate motion on a trackpoint with a physical
reference. Unlike mice or touchpads where the motion can be
measured in mm, the trackpoint only responds to pressure. Without special
equipment it is impossible to measure identical pressure values across
multiple laptops.�h]�h9X"  It is difficult to associate motion on a trackpoint with a physical
reference. Unlike mice or touchpads where the motion can be
measured in mm, the trackpoint only responds to pressure. Without special
equipment it is impossible to measure identical pressure values across
multiple laptops.�����}�(hj8  hj6  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK%hj%  hhubhE)��}�(hX�  The values provided by a trackpoint are motion deltas, usually corresponding
to the pressure applied to the trackstick. For example, pressure towards the
screen on a laptop provides negative y deltas. The reporting rate increases
as the pressure increases and once events are reported at the maximum rate,
the delta values increase. The figure below shows a rough illustration of
this concept. As the pressure
decreases, the delta decrease first, then the reporting rate until the
trackpoint is in a neutral state and no events are reported. Trackpoint data
is hard to generalize, see
`Observations on trackpoint input data
<a href="https://who-t.blogspot.com/2018/06/observations-on-trackpoint-input-data.html">`_
for more details.�h]�(h9XI  The values provided by a trackpoint are motion deltas, usually corresponding
to the pressure applied to the trackstick. For example, pressure towards the
screen on a laptop provides negative y deltas. The reporting rate increases
as the pressure increases and once events are reported at the maximum rate,
the delta values increase. The figure below shows a rough illustration of
this concept. As the pressure
decreases, the delta decrease first, then the reporting rate until the
trackpoint is in a neutral state and no events are reported. Trackpoint data
is hard to generalize, see
�����}�(hXI  The values provided by a trackpoint are motion deltas, usually corresponding
to the pressure applied to the trackstick. For example, pressure towards the
screen on a laptop provides negative y deltas. The reporting rate increases
as the pressure increases and once events are reported at the maximum rate,
the delta values increase. The figure below shows a rough illustration of
this concept. As the pressure
decreases, the delta decrease first, then the reporting rate until the
trackpoint is in a neutral state and no events are reported. Trackpoint data
is hard to generalize, see
�hjD  hhhNhNubh �	reference���)��}�(h��`Observations on trackpoint input data
<a href="https://who-t.blogspot.com/2018/06/observations-on-trackpoint-input-data.html">`_�h]�h9�%Observations on trackpoint input data�����}�(hhhjO  ubah}�(h]�h]�h]�h]�h]��name��%Observations on trackpoint input data��refuri��Uahref="https://who-t.blogspot.com/2018/06/observations-on-trackpoint-input-data.html"�uhjM  hjD  ubh)��}�(h�Y
<a href="https://who-t.blogspot.com/2018/06/observations-on-trackpoint-input-data.html">�h]�h}�(h]��%observations-on-trackpoint-input-data�ah]�h]��%observations on trackpoint input data�ah]�h]��refuri�j`  uhh�
referenced�KhjD  ubh9�
for more details.�����}�(h�
for more details.�hjD  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK+hj%  hhubhU)��}�(hhh]�(hZ)��}�(h��.. figure:: trackpoint-delta-illustration.svg
    :align: center

    Illustration of the relationship between reporting rate and delta values on a trackpoint
�h]�h}�(h]�h]�h]�h]�h]��uri��!trackpoint-delta-illustration.svg�hg}�hij�  suhhYhjz  hh,hK;ubhk)��}�(h�XIllustration of the relationship between reporting rate and delta values on a trackpoint�h]�h9�XIllustration of the relationship between reporting rate and delta values on a trackpoint�����}�(hj�  hj�  ubah}�(h]�h]�h]�h]�h]�uhhjhh,hK;hjz  ubeh}�(h]��id2�ah]�h]�h]�h]�h��center�uhhThK;hj%  hhhh,ubhE)��}�(hX  The delta range itself can vary greatly between laptops, some devices send a
maximum delta value of 30, others can go beyond 100. However, the useful
delta range is a fraction of the maximum range. It is uncomfortable to exert
sufficient pressure to even get close to the maximum ranges.�h]�h9X  The delta range itself can vary greatly between laptops, some devices send a
maximum delta value of 30, others can go beyond 100. However, the useful
delta range is a fraction of the maximum range. It is uncomfortable to exert
sufficient pressure to even get close to the maximum ranges.�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK=hj%  hhubhE)��}�(h�vlibinput provides a :ref:`Magic Trackpoint Multiplier
<trackpoint_multiplier>` to normalize the trackpoint input data.�h]�(h9�libinput provides a �����}�(h�libinput provides a �hj�  hhhNhNubh�)��}�(h�::ref:`Magic Trackpoint Multiplier
<trackpoint_multiplier>`�h]�h�)��}�(hj�  h]�h9�Magic Trackpoint Multiplier�����}�(hhhj�  ubah}�(h]�h]�(h��std��std-ref�eh]�h]�h]�uhh�hj�  ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�j�  �refexplicit��h��trackpoint_multiplier�h�h�h��uhh�hh,hKBhj�  ubh9�( to normalize the trackpoint input data.�����}�(h�( to normalize the trackpoint input data.�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKBhj%  hhubeh}�(h]�(�motion-range-on-trackpoints�j  eh]�h]�(�motion range on trackpoints��trackpoint_range�eh]�h]�uhh-hh/hhhh,hK#j!  }�j�  j  sj#  }�j  j  subeh}�(h]�(�trackpoints-and-pointing-sticks�h+eh]�h]�(�trackpoints and pointing sticks��trackpoints�eh]�h]�uhh-hhhhhh,hKj!  }�j�  h sj#  }�h+h subeh}�(h]�h]�h]�h]�h]��source�h,uhh�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(h2N�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}�(�git_version�h �substitution_definition���)��}�(h�,.. |git_version| replace:: :commit:`d2f4222`�h]�jN  )��}�(h�git commit d2f4222�h]�h9�git commit d2f4222�����}�(hhhjW  ubah}�(h]�h]�h]�h]�h]��internal���refuri��?https://gitlab.freedesktop.org/libinput/libinput/commit/d2f4222�uhjM  hjS  ubah}�(h]�h]�h]�jP  ah]�h]�uhjQ  h�<rst_prolog>�hKhhub�git_version_full�jR  )��}�(h�].. |git_version_full| replace:: :commit:`<function get_git_version_full at 0x7f7878870ea0>`

�h]�jN  )��}�(h�<git commit <function get_git_version_full at 0x7f7878870ea0>�h]�h9�<git commit <function get_git_version_full at 0x7f7878870ea0>�����}�(hhhjt  ubah}�(h]�h]�h]�h]�h]��internal���refuri��ihttps://gitlab.freedesktop.org/libinput/libinput/commit/<function get_git_version_full at 0x7f7878870ea0>�uhjM  hjp  ubah}�(h]�h]�h]�jo  ah]�h]�uhjQ  hjn  hKhhubu�substitution_names�}�(�git_version�jP  �git_version_full�jo  u�refnames�}��refids�}�(h+]�h ah�]�h�aj  ]�j  au�nameids�}�(j�  h+j�  j�  j  h�j  j  j�  j  j�  j�  jj  jg  u�	nametypes�}�(j�  �j�  Nj  �j  Nj�  �j�  Njj  �uh}�(h+h/j�  h/h�h�j  h�j  j%  j�  j%  jg  ja  h|hVj�  jz  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]��transform_messages�]�(h �system_message���)��}�(hhh]�hE)��}�(hhh]�h9�1Hyperlink target "trackpoints" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type��INFO��source�h,�line�Kuhj�  ubj�  )��}�(hhh]�hE)��}�(hhh]�h9�=Hyperlink target "trackpoint-buttonscroll" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h,�line�Kuhj�  ubj�  )��}�(hhh]�hE)��}�(hhh]�h9�6Hyperlink target "trackpoint-range" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h,�line�K$uhj�  ube�transformer�N�
decoration�Nhhub.