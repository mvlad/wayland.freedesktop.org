���0      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �block_quote���)��}�(hhh]�(h �substitution_definition���)��}�(h�,.. |git_version| replace:: :commit:`9da9118`�h]�h �	reference���)��}�(h�git commit 9da9118�h]�h �Text����git commit 9da9118�����}�(hh�parent�huba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]��internal���refuri��?https://gitlab.freedesktop.org/libinput/libinput/commit/9da9118�u�tagname�hh hubah!}�(h#]�h%]�h']��git_version�ah)]�h+]�uh0h�source��<rst_prolog>��line�Kh hubh)��}�(h�].. |git_version_full| replace:: :commit:`<function get_git_version_full at 0x7f81e92b1598>`

�h]�h)��}�(h�<git commit <function get_git_version_full at 0x7f81e92b1598>�h]�h�<git commit <function get_git_version_full at 0x7f81e92b1598>�����}�(hhh h?ubah!}�(h#]�h%]�h']�h)]�h+]��internal���refuri��ihttps://gitlab.freedesktop.org/libinput/libinput/commit/<function get_git_version_full at 0x7f81e92b1598>�uh0hh h;ubah!}�(h#]�h%]�h']��git_version_full�ah)]�h+]�uh0hh8h9h:Kh hubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h	h hhhh8Nh:Nubh �target���)��}�(h�.. _switches:�h]�h!}�(h#]�h%]�h']�h)]�h+]��refid��switches�uh0h]h:Kh hhhh8�4/home/whot/code/libinput/build/doc/user/switches.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�Switches�h]�h�Switches�����}�(hhuh hshhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh hnhhh8hkh:Kubh �	paragraph���)��}�(h��libinput supports the lid and tablet-mode switches. Unlike button events
that come in press and release pairs, switches are usually toggled once and
left at the setting for an extended period of time.�h]�h��libinput supports the lid and tablet-mode switches. Unlike button events
that come in press and release pairs, switches are usually toggled once and
left at the setting for an extended period of time.�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh�)��}�(hX  Only some switches are handled by libinput, see **libinput_switch** for a
list of supported switches. Switch events are exposed to the caller, but
libinput may handle some switch events internally and enable or disable
specific features based on a switch state.�h]�(h�0Only some switches are handled by libinput, see �����}�(h�0Only some switches are handled by libinput, see �h h�hhh8Nh:Nubh �strong���)��}�(h�**libinput_switch**�h]�h�libinput_switch�����}�(hhh h�ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h h�ubh�� for a
list of supported switches. Switch events are exposed to the caller, but
libinput may handle some switch events internally and enable or disable
specific features based on a switch state.�����}�(h�� for a
list of supported switches. Switch events are exposed to the caller, but
libinput may handle some switch events internally and enable or disable
specific features based on a switch state.�h h�hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh�)��}�(h��The order of switch events is guaranteed to be correct, i.e., a switch will
never send consecutive switch on, or switch off, events.�h]�h��The order of switch events is guaranteed to be correct, i.e., a switch will
never send consecutive switch on, or switch off, events.�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh^)��}�(h�.. _switches_lid:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�switches-lid�uh0h]h:Kh hnhhh8hkubhm)��}�(hhh]�(hr)��}�(h�Lid switch handling�h]�h�Lid switch handling�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh h�hhh8hkh:Kubh�)��}�(hX�  Where available, libinput listens to devices providing a lid switch.
The evdev event code ``EV_SW`` ``SW_LID`` is provided as
**LIBINPUT_SWITCH_LID**. If devices with a lid switch have a touchpad device,
the device is disabled while the lid is logically closed. This is to avoid
ghost touches that can be caused by interference with touchpads and the
closed lid. The touchpad is automatically re-enabled whenever the lid is
openend.�h]�(h�ZWhere available, libinput listens to devices providing a lid switch.
The evdev event code �����}�(h�ZWhere available, libinput listens to devices providing a lid switch.
The evdev event code �h h�hhh8Nh:Nubh �literal���)��}�(h�	``EV_SW``�h]�h�EV_SW�����}�(hhh h�ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h h�ubh� �����}�(h� �h h�hhh8Nh:Nubh�)��}�(h�
``SW_LID``�h]�h�SW_LID�����}�(hhh h�ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h h�ubh� is provided as
�����}�(h� is provided as
�h h�hhh8Nh:Nubh�)��}�(h�**LIBINPUT_SWITCH_LID**�h]�h�LIBINPUT_SWITCH_LID�����}�(hhh j  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h h�ubhX  . If devices with a lid switch have a touchpad device,
the device is disabled while the lid is logically closed. This is to avoid
ghost touches that can be caused by interference with touchpads and the
closed lid. The touchpad is automatically re-enabled whenever the lid is
openend.�����}�(hX  . If devices with a lid switch have a touchpad device,
the device is disabled while the lid is logically closed. This is to avoid
ghost touches that can be caused by interference with touchpads and the
closed lid. The touchpad is automatically re-enabled whenever the lid is
openend.�h h�hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh h�hhubh�)��}�(h��This handling of lid switches is transparent to the user, no notifications
are sent and the device appears as enabled at all times.�h]�h��This handling of lid switches is transparent to the user, no notifications
are sent and the device appears as enabled at all times.�����}�(hj+  h j)  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K!h h�hhubh�)��}�(hX  On some devices, the device's lid state does not always reflect the physical
state and the lid state may report as closed even when the lid is physicall
open. libinput employs some heuristics to detect user input (specificially
typing) to re-enable the touchpad on those devices.�h]�hX  On some devices, the device’s lid state does not always reflect the physical
state and the lid state may report as closed even when the lid is physicall
open. libinput employs some heuristics to detect user input (specificially
typing) to re-enable the touchpad on those devices.�����}�(hj9  h j7  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K$h h�hhubh^)��}�(h�.. _switches_tablet_mode:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�switches-tablet-mode�uh0h]h:K.h h�hhh8hkubeh!}�(h#]�(�lid-switch-handling�h�eh%]�h']�(�lid switch handling��switches_lid�eh)]�h+]�uh0hlh hnhhh8hkh:K�expect_referenced_by_name�}�jV  h�s�expect_referenced_by_id�}�h�h�subhm)��}�(hhh]�(hr)��}�(h�Tablet mode switch handling�h]�h�Tablet mode switch handling�����}�(hjb  h j`  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j]  hhh8hkh:K-ubh�)��}�(h��Where available, libinput listens to devices providing a tablet mode switch.
This switch is usually triggered on devices that can switch between a normal
laptop layout and a tablet-like layout. One example for such a device is the
Lenovo Yoga.�h]�h��Where available, libinput listens to devices providing a tablet mode switch.
This switch is usually triggered on devices that can switch between a normal
laptop layout and a tablet-like layout. One example for such a device is the
Lenovo Yoga.�����}�(hjp  h jn  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K/h j]  hhubh�)��}�(hXI  The event sent by the kernel is ``EV_SW`` ``SW_TABLET_MODE`` and is provided as
**LIBINPUT_SWITCH_TABLET_MODE**. When the device switches to tablet mode,
the touchpad and internal keyboard are disabled. If a trackpoint exists,
it is disabled too. The input devices are automatically re-enabled whenever
tablet mode is disengaged.�h]�(h� The event sent by the kernel is �����}�(h� The event sent by the kernel is �h j|  hhh8Nh:Nubh�)��}�(h�	``EV_SW``�h]�h�EV_SW�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j|  ubh� �����}�(hh�h j|  hhh8Nh:Nubh�)��}�(h�``SW_TABLET_MODE``�h]�h�SW_TABLET_MODE�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j|  ubh� and is provided as
�����}�(h� and is provided as
�h j|  hhh8Nh:Nubh�)��}�(h�**LIBINPUT_SWITCH_TABLET_MODE**�h]�h�LIBINPUT_SWITCH_TABLET_MODE�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j|  ubh��. When the device switches to tablet mode,
the touchpad and internal keyboard are disabled. If a trackpoint exists,
it is disabled too. The input devices are automatically re-enabled whenever
tablet mode is disengaged.�����}�(h��. When the device switches to tablet mode,
the touchpad and internal keyboard are disabled. If a trackpoint exists,
it is disabled too. The input devices are automatically re-enabled whenever
tablet mode is disengaged.�h j|  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K4h j]  hhubh�)��}�(h��This handling of tablet mode switches is transparent to the user, no
notifications are sent and the device appears as enabled at all times.�h]�h��This handling of tablet mode switches is transparent to the user, no
notifications are sent and the device appears as enabled at all times.�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K:h j]  hhubeh!}�(h#]�(�tablet-mode-switch-handling�jO  eh%]�h']�(�tablet mode switch handling��switches_tablet_mode�eh)]�h+]�uh0hlh hnhhh8hkh:K-jY  }�j�  jE  sj[  }�jO  jE  subeh!}�(h#]�(hj�id1�eh%]�h']��switches�ah)]��switches�ah+]�uh0hlh hhhh8hkh:K�
referenced�KjY  }�j�  h_sj[  }�hjh_subeh!}�(h#]�h%]�h']�h)]�h+]��source�hkuh0h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hqN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�hk�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�N�gettext_compact��ub�reporter�N�indirect_targets�]��substitution_defs�}�(h5hhTh;u�substitution_names�}�(�git_version�h5�git_version_full�hTu�refnames�}��refids�}�(hj]�h_ah�]�h�ajO  ]�jE  au�nameids�}�(j�  hjjV  h�jU  jR  j�  jO  j�  j�  u�	nametypes�}�(j�  �jV  �jU  Nj�  �j�  Nuh#}�(hjhnj�  hnh�h�jR  h�jO  j]  j�  j]  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]�h �system_message���)��}�(hhh]�h�)��}�(h�+Duplicate implicit target name: "switches".�h]�h�/Duplicate implicit target name: “switches”.�����}�(hhh jm  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h jj  ubah!}�(h#]�h%]�h']�h)]�h+]�j�  a�level�K�type��INFO��source�hk�line�Kuh0jh  h hnhhh8hkh:Kuba�transform_messages�]�(ji  )��}�(hhh]�h�)��}�(hhh]�h�.Hyperlink target "switches" is not referenced.�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j�  �source�hk�line�Kuh0jh  ubji  )��}�(hhh]�h�)��}�(hhh]�h�2Hyperlink target "switches-lid" is not referenced.�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j�  �source�hk�line�Kuh0jh  ubji  )��}�(hhh]�h�)��}�(hhh]�h�:Hyperlink target "switches-tablet-mode" is not referenced.�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j�  �source�hk�line�K.uh0jh  ube�transformer�N�
decoration�Nhhub.