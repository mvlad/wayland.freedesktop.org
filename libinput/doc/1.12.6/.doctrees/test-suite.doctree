��\      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �block_quote���)��}�(hhh]�(h �substitution_definition���)��}�(h�,.. |git_version| replace:: :commit:`9da9118`�h]�h �	reference���)��}�(h�git commit 9da9118�h]�h �Text����git commit 9da9118�����}�(hh�parent�huba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]��internal���refuri��?https://gitlab.freedesktop.org/libinput/libinput/commit/9da9118�u�tagname�hh hubah!}�(h#]�h%]�h']��git_version�ah)]�h+]�uh0h�source��<rst_prolog>��line�Kh hubh)��}�(h�].. |git_version_full| replace:: :commit:`<function get_git_version_full at 0x7f81e92b1598>`

�h]�h)��}�(h�<git commit <function get_git_version_full at 0x7f81e92b1598>�h]�h�<git commit <function get_git_version_full at 0x7f81e92b1598>�����}�(hhh h?ubah!}�(h#]�h%]�h']�h)]�h+]��internal���refuri��ihttps://gitlab.freedesktop.org/libinput/libinput/commit/<function get_git_version_full at 0x7f81e92b1598>�uh0hh h;ubah!}�(h#]�h%]�h']��git_version_full�ah)]�h+]�uh0hh8h9h:Kh hubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h	h hhhh8Nh:Nubh �target���)��}�(h�.. _test-suite:�h]�h!}�(h#]�h%]�h']�h)]�h+]��refid��
test-suite�uh0h]h:Kh hhhh8�6/home/whot/code/libinput/build/doc/user/test-suite.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�libinput test suite�h]�h�libinput test suite�����}�(hhuh hshhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh hnhhh8hkh:Kubh �	paragraph���)��}�(h��libinput ships with a number of tests all run automatically on ``ninja test``.
The primary test suite is the ``libinput-test-suite-runner``. When testing,
the ``libinput-test-suite-runner`` should always be invoked to check for
behavior changes.�h]�(h�?libinput ships with a number of tests all run automatically on �����}�(h�?libinput ships with a number of tests all run automatically on �h h�hhh8Nh:Nubh �literal���)��}�(h�``ninja test``�h]�h�
ninja test�����}�(hhh h�ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h h�ubh� .
The primary test suite is the �����}�(h� .
The primary test suite is the �h h�hhh8Nh:Nubh�)��}�(h�``libinput-test-suite-runner``�h]�h�libinput-test-suite-runner�����}�(hhh h�ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h h�ubh�. When testing,
the �����}�(h�. When testing,
the �h h�hhh8Nh:Nubh�)��}�(h�``libinput-test-suite-runner``�h]�h�libinput-test-suite-runner�����}�(hhh h�ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h h�ubh�8 should always be invoked to check for
behavior changes.�����}�(h�8 should always be invoked to check for
behavior changes.�h h�hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh�)��}�(h��The test suite runner uses
`Check <http://check.sourceforge.net/doc/check_html/>`_ underneath the hood
but most of the functionality is abstracted into *litest* wrappers.�h]�(h�The test suite runner uses
�����}�(h�The test suite runner uses
�h h�hhh8Nh:Nubh)��}�(h�7`Check <http://check.sourceforge.net/doc/check_html/>`_�h]�h�Check�����}�(hhh h�ubah!}�(h#]�h%]�h']�h)]�h+]��name��Check��refuri��,http://check.sourceforge.net/doc/check_html/�uh0hh h�ubh^)��}�(h�/ <http://check.sourceforge.net/doc/check_html/>�h]�h!}�(h#]��check�ah%]�h']��check�ah)]�h+]��refuri�h�uh0h]�
referenced�Kh h�ubh�F underneath the hood
but most of the functionality is abstracted into �����}�(h�F underneath the hood
but most of the functionality is abstracted into �h h�hhh8Nh:Nubh �emphasis���)��}�(h�*litest*�h]�h�litest�����}�(hhh h�ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h h�ubh�
 wrappers.�����}�(h�
 wrappers.�h h�hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh�)��}�(hX�  The test suite runner has a make-like job control enabled by the ``-j`` or
``--jobs`` flag and will fork off as many parallel processes as given by this
flag. The default if unspecified is 8. When debugging a specific test case
failure it is recommended to employ test filtures (see :ref:`test-filtering`)
and disable parallel tests. The test suite automatically disables parallel
make when run in gdb.�h]�(h�AThe test suite runner has a make-like job control enabled by the �����}�(h�AThe test suite runner has a make-like job control enabled by the �h j  hhh8Nh:Nubh�)��}�(h�``-j``�h]�h�-j�����}�(hhh j  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j  ubh� or
�����}�(h� or
�h j  hhh8Nh:Nubh�)��}�(h�
``--jobs``�h]�h�--jobs�����}�(hhh j2  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j  ubh�� flag and will fork off as many parallel processes as given by this
flag. The default if unspecified is 8. When debugging a specific test case
failure it is recommended to employ test filtures (see �����}�(h�� flag and will fork off as many parallel processes as given by this
flag. The default if unspecified is 8. When debugging a specific test case
failure it is recommended to employ test filtures (see �h j  hhh8Nh:Nub�sphinx.addnodes��pending_xref���)��}�(h�:ref:`test-filtering`�h]�h �inline���)��}�(hjJ  h]�h�test-filtering�����}�(hhh jN  ubah!}�(h#]�h%]�(�xref��std��std-ref�eh']�h)]�h+]�uh0jL  h jH  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�jY  �refexplicit���	reftarget��test-filtering��refdoc��
test-suite��refwarn��uh0jF  h8hkh:Kh j  ubh�b)
and disable parallel tests. The test suite automatically disables parallel
make when run in gdb.�����}�(h�b)
and disable parallel tests. The test suite automatically disables parallel
make when run in gdb.�h j  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh^)��}�(h�.. _test-config:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�test-config�uh0h]h:Kh hnhhh8hkubhm)��}�(hhh]�(hr)��}�(h�"X.Org config to avoid interference�h]�h�"X.Org config to avoid interference�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j�  hhh8hkh:Kubh�)��}�(h��uinput devices created by the test suite are usually recognised by X as
input devices. All events sent through these devices will generate X events
and interfere with your desktop.�h]�h��uinput devices created by the test suite are usually recognised by X as
input devices. All events sent through these devices will generate X events
and interfere with your desktop.�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh j�  hhubh�)��}�(h��Copy the file ``$srcdir/test/50-litest.conf`` into your ``/etc/X11/xorg.conf.d``
and restart X. This will ignore any litest devices and thus not interfere
with your desktop.�h]�(h�Copy the file �����}�(h�Copy the file �h j�  hhh8Nh:Nubh�)��}�(h�``$srcdir/test/50-litest.conf``�h]�h�$srcdir/test/50-litest.conf�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubh� into your �����}�(h� into your �h j�  hhh8Nh:Nubh�)��}�(h�``/etc/X11/xorg.conf.d``�h]�h�/etc/X11/xorg.conf.d�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubh�]
and restart X. This will ignore any litest devices and thus not interfere
with your desktop.�����}�(h�]
and restart X. This will ignore any litest devices and thus not interfere
with your desktop.�h j�  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K!h j�  hhubh^)��}�(h�.. _test-root:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�	test-root�uh0h]h:K*h j�  hhh8hkubeh!}�(h#]�(�"x-org-config-to-avoid-interference�j�  eh%]�h']�(�"x.org config to avoid interference��test-config�eh)]�h+]�uh0hlh hnhhh8hkh:K�expect_referenced_by_name�}�j�  jx  s�expect_referenced_by_id�}�j�  jx  subhm)��}�(hhh]�(hr)��}�(h�!Permissions required to run tests�h]�h�!Permissions required to run tests�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j�  hhh8hkh:K)ubh�)��}�(hX1  Most tests require the creation of uinput devices and access to the
resulting ``/dev/input/eventX`` nodes. Some tests require temporary udev rules.
**This usually requires the tests to be run as root**. If not run as
root, the test suite runner will exit with status 77, interpreted as
"skipped" by ninja.�h]�(h�NMost tests require the creation of uinput devices and access to the
resulting �����}�(h�NMost tests require the creation of uinput devices and access to the
resulting �h j   hhh8Nh:Nubh�)��}�(h�``/dev/input/eventX``�h]�h�/dev/input/eventX�����}�(hhh j	  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j   ubh�1 nodes. Some tests require temporary udev rules.
�����}�(h�1 nodes. Some tests require temporary udev rules.
�h j   hhh8Nh:Nubh �strong���)��}�(h�5**This usually requires the tests to be run as root**�h]�h�1This usually requires the tests to be run as root�����}�(hhh j  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j  h j   ubh�l. If not run as
root, the test suite runner will exit with status 77, interpreted as
“skipped” by ninja.�����}�(h�h. If not run as
root, the test suite runner will exit with status 77, interpreted as
"skipped" by ninja.�h j   hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K+h j�  hhubh^)��}�(h�.. _test-filtering:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�test-filtering�uh0h]h:K6h j�  hhh8hkubeh!}�(h#]�(�!permissions-required-to-run-tests�j�  eh%]�h']�(�!permissions required to run tests��	test-root�eh)]�h+]�uh0hlh hnhhh8hkh:K)j�  }�jH  j�  sj�  }�j�  j�  subhm)��}�(hhh]�(hr)��}�(h�Selective running of tests�h]�h�Selective running of tests�����}�(hjR  h jP  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh jM  hhh8hkh:K5ubh�)��}�(hX�  litest's tests are grouped into test groups, test names and devices. A test
group is e.g.  "touchpad:tap" and incorporates all tapping-related tests for
touchpads. Each test function is (usually) run with one or more specific
devices. The ``--list`` commandline argument shows the list of suites and
tests. This is useful when trying to figure out if a specific test is
run for a device.�h]�(h��litest’s tests are grouped into test groups, test names and devices. A test
group is e.g.  “touchpad:tap” and incorporates all tapping-related tests for
touchpads. Each test function is (usually) run with one or more specific
devices. The �����}�(h��litest's tests are grouped into test groups, test names and devices. A test
group is e.g.  "touchpad:tap" and incorporates all tapping-related tests for
touchpads. Each test function is (usually) run with one or more specific
devices. The �h j^  hhh8Nh:Nubh�)��}�(h�
``--list``�h]�h�--list�����}�(hhh jg  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j^  ubh�� commandline argument shows the list of suites and
tests. This is useful when trying to figure out if a specific test is
run for a device.�����}�(h�� commandline argument shows the list of suites and
tests. This is useful when trying to figure out if a specific test is
run for a device.�h j^  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K7h jM  hhubh �literal_block���)��}�(hX�  $ ./test/libinput-test-suite-runner --list
...
pointer:left-handed:
   pointer_left_handed_during_click_multiple_buttons:
           trackpoint
           ms-surface-cover
           mouse-wheelclickcount
           mouse-wheelclickangle
           low-dpi-mouse
           mouse-roccat
           mouse-wheel-tilt
           mouse
           logitech-trackball
           cyborg-rat
           magicmouse
   pointer_left_handed_during_click:
           trackpoint
           ms-surface-cover
           mouse-wheelclickcount
           mouse-wheelclickangle
           low-dpi-mouse
           mouse-roccat
           mouse-wheel-tilt
           mouse
           logitech-trackball
           cyborg-rat
           litest-magicmouse-device
   pointer_left_handed:
           trackpoint
           ms-surface-cover
           mouse-wheelclickcount
           mouse-wheelclickangle
           low-dpi-mouse
           mouse-roccat
           mouse-wheel-tilt
           mouse
...�h]�hX�  $ ./test/libinput-test-suite-runner --list
...
pointer:left-handed:
   pointer_left_handed_during_click_multiple_buttons:
           trackpoint
           ms-surface-cover
           mouse-wheelclickcount
           mouse-wheelclickangle
           low-dpi-mouse
           mouse-roccat
           mouse-wheel-tilt
           mouse
           logitech-trackball
           cyborg-rat
           magicmouse
   pointer_left_handed_during_click:
           trackpoint
           ms-surface-cover
           mouse-wheelclickcount
           mouse-wheelclickangle
           low-dpi-mouse
           mouse-roccat
           mouse-wheel-tilt
           mouse
           logitech-trackball
           cyborg-rat
           litest-magicmouse-device
   pointer_left_handed:
           trackpoint
           ms-surface-cover
           mouse-wheelclickcount
           mouse-wheelclickangle
           low-dpi-mouse
           mouse-roccat
           mouse-wheel-tilt
           mouse
...�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]��	xml:space��preserve�uh0j�  h:KFh jM  hhh8hkubh�)��}�(hX,  In the above example, the "pointer:left-handed" suite contains multiple
tests, e.g. "pointer_left_handed_during_click" (this is also the function
name of the test, making it easy to grep for). This particular test is run
for various devices including the trackpoint device and the magic mouse
device.�h]�hX4  In the above example, the “pointer:left-handed” suite contains multiple
tests, e.g. “pointer_left_handed_during_click” (this is also the function
name of the test, making it easy to grep for). This particular test is run
for various devices including the trackpoint device and the magic mouse
device.�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Khh jM  hhubh�)��}�(h��The "no device" entry signals that litest does not instantiate a uinput
device for a specific test (though the test itself may
instantiate one).�h]�h��The “no device” entry signals that litest does not instantiate a uinput
device for a specific test (though the test itself may
instantiate one).�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Knh jM  hhubh�)��}�(h��The ``--filter-test`` argument enables selective running of tests through
basic shell-style function name matching. For example:�h]�(h�The �����}�(h�The �h j�  hhh8Nh:Nubh�)��}�(h�``--filter-test``�h]�h�--filter-test�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubh�k argument enables selective running of tests through
basic shell-style function name matching. For example:�����}�(h�k argument enables selective running of tests through
basic shell-style function name matching. For example:�h j�  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Krh jM  hhubj�  )��}�(h�=$ ./test/libinput-test-suite-runner --filter-test="*1fg_tap*"�h]�h�=$ ./test/libinput-test-suite-runner --filter-test="*1fg_tap*"�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�j�  j�  uh0j�  h:K}h jM  hhh8hkubh�)��}�(h��The ``--filter-device`` argument enables selective running of tests through
basic shell-style device name matching. The device names matched are the
litest-specific shortnames, see the output of ``--list``. For example:�h]�(h�The �����}�(h�The �h j�  hhh8Nh:Nubh�)��}�(h�``--filter-device``�h]�h�--filter-device�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubh�� argument enables selective running of tests through
basic shell-style device name matching. The device names matched are the
litest-specific shortnames, see the output of �����}�(h�� argument enables selective running of tests through
basic shell-style device name matching. The device names matched are the
litest-specific shortnames, see the output of �h j�  hhh8Nh:Nubh�)��}�(h�
``--list``�h]�h�--list�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubh�. For example:�����}�(h�. For example:�h j�  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K{h jM  hhubj�  )��}�(h�@$ ./test/libinput-test-suite-runner --filter-device="synaptics*"�h]�h�@$ ./test/libinput-test-suite-runner --filter-device="synaptics*"�����}�(hhh j  ubah!}�(h#]�h%]�h']�h)]�h+]�j�  j�  uh0j�  h:K�h jM  hhh8hkubh�)��}�(h��The ``--filter-group`` argument enables selective running of test groups
through basic shell-style test group matching. The test groups matched are
litest-specific test groups, see the output of ``--list``. For example:�h]�(h�The �����}�(h�The �h j!  hhh8Nh:Nubh�)��}�(h�``--filter-group``�h]�h�--filter-group�����}�(hhh j*  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j!  ubh�� argument enables selective running of test groups
through basic shell-style test group matching. The test groups matched are
litest-specific test groups, see the output of �����}�(h�� argument enables selective running of test groups
through basic shell-style test group matching. The test groups matched are
litest-specific test groups, see the output of �h j!  hhh8Nh:Nubh�)��}�(h�
``--list``�h]�h�--list�����}�(hhh j=  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j!  ubh�. For example:�����}�(h�. For example:�h j!  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h jM  hhubj�  )��}�(h�E$ ./test/libinput-test-suite-runner --filter-group="touchpad:*hover*"�h]�h�E$ ./test/libinput-test-suite-runner --filter-group="touchpad:*hover*"�����}�(hhh jV  ubah!}�(h#]�h%]�h']�h)]�h+]�j�  j�  uh0j�  h:K�h jM  hhh8hkubh�)��}�(h��The ``--filter-device`` and ``--filter-group`` arguments can be combined with
``--list`` to show which groups and devices will be affected.�h]�(h�The �����}�(h�The �h jd  hhh8Nh:Nubh�)��}�(h�``--filter-device``�h]�h�--filter-device�����}�(hhh jm  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h jd  ubh� and �����}�(h� and �h jd  hhh8Nh:Nubh�)��}�(h�``--filter-group``�h]�h�--filter-group�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h jd  ubh�  arguments can be combined with
�����}�(h�  arguments can be combined with
�h jd  hhh8Nh:Nubh�)��}�(h�
``--list``�h]�h�--list�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h jd  ubh�3 to show which groups and devices will be affected.�����}�(h�3 to show which groups and devices will be affected.�h jd  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h jM  hhubh^)��}�(h�.. _test-verbosity:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�test-verbosity�uh0h]h:K�h jM  hhh8hkubeh!}�(h#]�(�selective-running-of-tests�jA  eh%]�h']�(�selective running of tests��test-filtering�eh)]�h+]�uh0hlh hnhhh8hkh:K5j�  }�j�  j7  sj�  }�jA  j7  subhm)��}�(hhh]�(hr)��}�(h�Controlling test output�h]�h�Controlling test output�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j�  hhh8hkh:K�ubh�)��}�(h��Each test supports the ``--verbose`` commandline option to enable debugging
output, see **libinput_log_set_priority()** for details. The ``LITEST_VERBOSE``
environment variable, if set, also enables verbose mode.�h]�(h�Each test supports the �����}�(h�Each test supports the �h j�  hhh8Nh:Nubh�)��}�(h�``--verbose``�h]�h�	--verbose�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubh�4 commandline option to enable debugging
output, see �����}�(h�4 commandline option to enable debugging
output, see �h j�  hhh8Nh:Nubj  )��}�(h�**libinput_log_set_priority()**�h]�h�libinput_log_set_priority()�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j  h j�  ubh� for details. The �����}�(h� for details. The �h j�  hhh8Nh:Nubh�)��}�(h�``LITEST_VERBOSE``�h]�h�LITEST_VERBOSE�����}�(hhh j  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubh�9
environment variable, if set, also enables verbose mode.�����}�(h�9
environment variable, if set, also enables verbose mode.�h j�  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h j�  hhubj�  )��}�(h�K$ ./test/libinput-test-suite-runner --verbose
$ LITEST_VERBOSE=1 ninja test�h]�h�K$ ./test/libinput-test-suite-runner --verbose
$ LITEST_VERBOSE=1 ninja test�����}�(hhh j  ubah!}�(h#]�h%]�h']�h)]�h+]�j�  j�  uh0j�  h:K�h j�  hhh8hkubeh!}�(h#]�(�controlling-test-output�j�  eh%]�h']�(�controlling test output��test-verbosity�eh)]�h+]�uh0hlh hnhhh8hkh:K�j�  }�j/  j�  sj�  }�j�  j�  subeh!}�(h#]�(�libinput-test-suite�hjeh%]�h']�(�libinput test suite��
test-suite�eh)]�h+]�uh0hlh hhhh8hkh:Kj�  }�j:  h_sj�  }�hjh_subeh!}�(h#]�h%]�h']�h)]�h+]��source�hkuh0h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hqN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�jb  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�hk�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�N�gettext_compact��ub�reporter�N�indirect_targets�]��substitution_defs�}�(h5hhTh;u�substitution_names�}�(�git_version�h5�git_version_full�hTu�refnames�}��refids�}�(hj]�h_aj�  ]�jx  aj�  ]�j�  ajA  ]�j7  aj�  ]�j�  au�nameids�}�(j:  hjj9  j6  h�h�j�  j�  j�  j�  jH  j�  jG  jD  j�  jA  j�  j�  j/  j�  j.  j+  u�	nametypes�}�(j:  �j9  Nh�j�  �j�  NjH  �jG  Nj�  �j�  Nj/  �j.  Nuh#}�(hjhnj6  hnh�h�j�  j�  j�  j�  j�  j�  jD  j�  jA  jM  j�  jM  j�  j�  j+  j�  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]��transform_messages�]�(h �system_message���)��}�(hhh]�h�)��}�(hhh]�h�0Hyperlink target "test-suite" is not referenced.�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type��INFO��source�hk�line�Kuh0j�  ubj�  )��}�(hhh]�h�)��}�(hhh]�h�1Hyperlink target "test-config" is not referenced.�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j�  �source�hk�line�Kuh0j�  ubj�  )��}�(hhh]�h�)��}�(hhh]�h�/Hyperlink target "test-root" is not referenced.�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j�  �source�hk�line�K*uh0j�  ubj�  )��}�(hhh]�h�)��}�(hhh]�h�4Hyperlink target "test-filtering" is not referenced.�����}�(hhh j  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j�  �source�hk�line�K6uh0j�  ubj�  )��}�(hhh]�h�)��}�(hhh]�h�4Hyperlink target "test-verbosity" is not referenced.�����}�(hhh j1  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j.  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j�  �source�hk�line�K�uh0j�  ube�transformer�N�
decoration�Nhhub.