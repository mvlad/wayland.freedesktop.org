���I      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �block_quote���)��}�(hhh]��
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�h	�parent�hhh�source�N�line�Nubh �target���)��}�(h�.. _t440_support:�h]�h}�(h]�h]�h]�h]�h]��refid��t440-support�uhhhKhhhhh�8/home/whot/code/libinput/build/doc/user/t440-support.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�#Lenovo \*40 series touchpad support�h]�h �Text����"Lenovo *40 series touchpad support�����}�(h�#Lenovo \*40 series touchpad support�hh4hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hh/hhhh,hKubh �	paragraph���)��}�(h�TThe Lenovo \*40 series emulates trackstick buttons on the top part of the
touchpads.�h]�h9�SThe Lenovo *40 series emulates trackstick buttons on the top part of the
touchpads.�����}�(h�TThe Lenovo \*40 series emulates trackstick buttons on the top part of the
touchpads.�hhGhhhNhNubah}�(h]�h]�h]�h]�h]�uhhEhh,hKhh/hhubh)��}�(h�.. _t440_support_overview:�h]�h}�(h]�h]�h]�h]�h]�h*�t440-support-overview�uhhhKhh/hhhh,ubh.)��}�(hhh]�(h3)��}�(h�Overview�h]�h9�Overview�����}�(hhfhhdhhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hhahhhh,hKubhF)��}�(hX�  The Lenovo \*40 series introduced a new type of touchpad. Previously, all
laptops had a separate set of physical buttons for the
`trackstick <http://en.wikipedia.org/wiki/Pointing_stick>`_. This
series removed these buttons, relying on a software emulation of the top
section of the touchpad. This is visually marked on the trackpad itself,
and clicks can be triggered by pressing the touchpad down with a finger in
the respective area:�h]�(h9��The Lenovo *40 series introduced a new type of touchpad. Previously, all
laptops had a separate set of physical buttons for the
�����}�(h��The Lenovo \*40 series introduced a new type of touchpad. Previously, all
laptops had a separate set of physical buttons for the
�hhrhhhNhNubh �	reference���)��}�(h�;`trackstick <http://en.wikipedia.org/wiki/Pointing_stick>`_�h]�h9�
trackstick�����}�(hhhh}ubah}�(h]�h]�h]�h]�h]��name��
trackstick��refuri��+http://en.wikipedia.org/wiki/Pointing_stick�uhh{hhrubh)��}�(h�. <http://en.wikipedia.org/wiki/Pointing_stick>�h]�h}�(h]��
trackstick�ah]�h]��
trackstick�ah]�h]��refuri�h�uhh�
referenced�Khhrubh9��. This
series removed these buttons, relying on a software emulation of the top
section of the touchpad. This is visually marked on the trackpad itself,
and clicks can be triggered by pressing the touchpad down with a finger in
the respective area:�����}�(h��. This
series removed these buttons, relying on a software emulation of the top
section of the touchpad. This is visually marked on the trackpad itself,
and clicks can be triggered by pressing the touchpad down with a finger in
the respective area:�hhrhhhNhNubeh}�(h]�h]�h]�h]�h]�uhhEhh,hKhhahhubh �figure���)��}�(hhh]�(h �image���)��}�(h��.. figure:: top-software-buttons.svg
    :align: center

    Left, right and middle-button click with top software button areas
�h]�h}�(h]�h]�h]�h]�h]��uri��top-software-buttons.svg��
candidates�}��*�h�suhh�hh�hh,hKubh �caption���)��}�(h�BLeft, right and middle-button click with top software button areas�h]�h9�BLeft, right and middle-button click with top software button areas�����}�(hh�hh�ubah}�(h]�h]�h]�h]�h]�uhh�hh,hKhh�ubeh}�(h]��id1�ah]�h]�h]�h]��align��center�uhh�hKhhahhhh,ubhF)��}�(h��This page only covers the top software buttons, the bottom button behavior
is covered in :ref:`Clickpad software buttons <clickpad_softbuttons>`.�h]�(h9�YThis page only covers the top software buttons, the bottom button behavior
is covered in �����}�(h�YThis page only covers the top software buttons, the bottom button behavior
is covered in �hh�hhhNhNub�sphinx.addnodes��pending_xref���)��}�(h�7:ref:`Clickpad software buttons <clickpad_softbuttons>`�h]�h �inline���)��}�(hh�h]�h9�Clickpad software buttons�����}�(hhhh�ubah}�(h]�h]�(�xref��std��std-ref�eh]�h]�h]�uhh�hh�ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�h�refexplicit���	reftarget��clickpad_softbuttons��refdoc��t440-support��refwarn��uhh�hh,hKhh�ubh9�.�����}�(h�.�hh�hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhEhh,hKhhahhubhF)��}�(h��Clickpads with a top button area are marked with the
`INPUT_PROP_TOPBUTTONPAD <https://www.kernel.org/doc/Documentation/input/event-codes.txt>`_
property.�h]�(h9�5Clickpads with a top button area are marked with the
�����}�(h�5Clickpads with a top button area are marked with the
�hj  hhhNhNubh|)��}�(h�[`INPUT_PROP_TOPBUTTONPAD <https://www.kernel.org/doc/Documentation/input/event-codes.txt>`_�h]�h9�INPUT_PROP_TOPBUTTONPAD�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]��name��INPUT_PROP_TOPBUTTONPAD�h��>https://www.kernel.org/doc/Documentation/input/event-codes.txt�uhh{hj  ubh)��}�(h�A <https://www.kernel.org/doc/Documentation/input/event-codes.txt>�h]�h}�(h]��input-prop-topbuttonpad�ah]�h]��input_prop_topbuttonpad�ah]�h]��refuri�j,  uhhh�Khj  ubh9�

property.�����}�(h�

property.�hj  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhEhh,hK hhahhubh)��}�(h�.. _t440_support_btn_size:�h]�h}�(h]�h]�h]�h]�h]�h*�t440-support-btn-size�uhhhK)hhahhhh,ubeh}�(h]�(�overview�h`eh]�h]�(�overview��t440_support_overview�eh]�h]�uhh-hh/hhhh,hK�expect_referenced_by_name�}�jV  hVs�expect_referenced_by_id�}�h`hVsubh.)��}�(hhh]�(h3)��}�(h�Size of the buttons�h]�h9�Size of the buttons�����}�(hjb  hj`  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj]  hhhh,hK(ubhF)��}�(h��The size of the buttons matches the visual markings on this touchpad.
The width of the left and right buttons is approximately 42% of the
touchpad's width, the middle button is centered and assigned 16% of the
touchpad width.�h]�h9��The size of the buttons matches the visual markings on this touchpad.
The width of the left and right buttons is approximately 42% of the
touchpad’s width, the middle button is centered and assigned 16% of the
touchpad width.�����}�(hjp  hjn  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhEhh,hK*hj]  hhubhF)��}�(h��The line of the buttons is 5mm from the top edge of the touchpad,
measurements of button presses showed that the size of the buttons needs to
be approximately 10mm high to work reliable (especially when using the
thumb to press the button).�h]�h9��The line of the buttons is 5mm from the top edge of the touchpad,
measurements of button presses showed that the size of the buttons needs to
be approximately 10mm high to work reliable (especially when using the
thumb to press the button).�����}�(hj~  hj|  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhEhh,hK/hj]  hhubh)��}�(h�.. _t440_support_btn_behavior:�h]�h}�(h]�h]�h]�h]�h]�h*�t440-support-btn-behavior�uhhhK9hj]  hhhh,ubeh}�(h]�(�size-of-the-buttons�jO  eh]�h]�(�size of the buttons��t440_support_btn_size�eh]�h]�uhh-hh/hhhh,hK(jY  }�j�  jE  sj[  }�jO  jE  subh.)��}�(hhh]�(h3)��}�(h�Button behavior�h]�h9�Button behavior�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj�  hhhh,hK8ubhF)��}�(hX+  Movement in the top button area does not generate pointer movement. These
buttons are not replacement buttons for the bottom button area but have
their own behavior. Semantically attached to the trackstick device, libinput
re-routes events from these buttons to appear through the trackstick device.�h]�h9X+  Movement in the top button area does not generate pointer movement. These
buttons are not replacement buttons for the bottom button area but have
their own behavior. Semantically attached to the trackstick device, libinput
re-routes events from these buttons to appear through the trackstick device.�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhEhh,hK:hj�  hhub�sphinx.ext.graphviz��graphviz���)��}�(hhh]�h}�(h]�h]�h]�h]�h]��code�X�  digraph top_button_routing
{
    rankdir="LR";
    node [shape="box";]

    trackstick [label="trackstick kernel device"];
    touchpad [label="touchpad kernel device"];

    subgraph cluster0 {
            bgcolor = floralwhite
            label = "libinput"

            libinput_ts [label="trackstick libinput_device"
                         style=filled
                         fillcolor=white];
            libinput_tp [label="touchpad libinput_device"
                         style=filled
                         fillcolor=white];

            libinput_tp -> libinput_ts [constraint=false
                                        color="red4"];
    }

    trackstick -> libinput_ts [arrowhead="none"]
    touchpad -> libinput_tp [color="red4"]

    events_tp [label="other touchpad events"];
    events_topbutton [label="top software button events"];

    libinput_tp -> events_tp [arrowhead="none"]
    libinput_ts -> events_topbutton [color="red4"]
}��options�}��docname�j  suhj�  hj�  hhhh,hKeubhF)��}�(hX�  The top button areas work even if the touchpad is disabled but will be
disabled when the trackstick device is disabled. If the finger starts inside
the top area and moves outside the button area the finger is treated as dead
and must be lifted to generate future buttons.  Likewise, movement into the
top button area does not trigger button events, a click has to start inside
this area to take effect.�h]�h9X�  The top button areas work even if the touchpad is disabled but will be
disabled when the trackstick device is disabled. If the finger starts inside
the top area and moves outside the button area the finger is treated as dead
and must be lifted to generate future buttons.  Likewise, movement into the
top button area does not trigger button events, a click has to start inside
this area to take effect.�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhEhh,hKfhj�  hhubh)��}�(h� .. _t440_support_identification:�h]�h}�(h]�h]�h]�h]�h]�h*�t440-support-identification�uhhhKrhj�  hhhh,ubeh}�(h]�(�button-behavior�j�  eh]�h]�(�button behavior��t440_support_btn_behavior�eh]�h]�uhh-hh/hhhh,hK8jY  }�j�  j�  sj[  }�j�  j�  subh.)��}�(hhh]�(h3)��}�(h�Kernel support�h]�h9�Kernel support�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj�  hhhh,hKqubhF)��}�(hX  The firmware on the first generation of touchpads providing top software
buttons is buggy and announces wrong ranges.
`Kernel patches <https://lkml.org/lkml/2014/3/7/722>`_ are required;
these fixes are available in kernels 3.14.1, 3.15 and later but each
touchpad needs a separate fix.�h]�(h9�vThe firmware on the first generation of touchpads providing top software
buttons is buggy and announces wrong ranges.
�����}�(h�vThe firmware on the first generation of touchpads providing top software
buttons is buggy and announces wrong ranges.
�hj  hhhNhNubh|)��}�(h�6`Kernel patches <https://lkml.org/lkml/2014/3/7/722>`_�h]�h9�Kernel patches�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]��name��Kernel patches�h��"https://lkml.org/lkml/2014/3/7/722�uhh{hj  ubh)��}�(h�% <https://lkml.org/lkml/2014/3/7/722>�h]�h}�(h]��kernel-patches�ah]�h]��kernel patches�ah]�h]��refuri�j  uhhh�Khj  ubh9�r are required;
these fixes are available in kernels 3.14.1, 3.15 and later but each
touchpad needs a separate fix.�����}�(h�r are required;
these fixes are available in kernels 3.14.1, 3.15 and later but each
touchpad needs a separate fix.�hj  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhEhh,hKshj�  hhubhF)��}�(hX  The October 2014 refresh of these laptops do not have this firmware bug
anymore and should work without per-device patches, though
`this kernel commit <http://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=02e07492cdfae9c86e3bd21c0beec88dbcc1e9e8>`_
is required.�h]�(h9��The October 2014 refresh of these laptops do not have this firmware bug
anymore and should work without per-device patches, though
�����}�(h��The October 2014 refresh of these laptops do not have this firmware bug
anymore and should work without per-device patches, though
�hj7  hhhNhNubh|)��}�(h��`this kernel commit <http://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=02e07492cdfae9c86e3bd21c0beec88dbcc1e9e8>`_�h]�h9�this kernel commit�����}�(hhhj@  ubah}�(h]�h]�h]�h]�h]��name��this kernel commit�h��rhttp://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=02e07492cdfae9c86e3bd21c0beec88dbcc1e9e8�uhh{hj7  ubh)��}�(h�u <http://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=02e07492cdfae9c86e3bd21c0beec88dbcc1e9e8>�h]�h}�(h]��this-kernel-commit�ah]�h]��this kernel commit�ah]�h]��refuri�jP  uhhh�Khj7  ubh9�
is required.�����}�(h�
is required.�hj7  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhEhh,hKyhj�  hhubhF)��}�(h��For a complete list of supported touchpads check
`the kernel source <http://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/tree/drivers/input/mouse/synaptics.c>`_
(search for "topbuttonpad_pnp_ids").�h]�(h9�1For a complete list of supported touchpads check
�����}�(h�1For a complete list of supported touchpads check
�hji  hhhNhNubh|)��}�(h�z`the kernel source <http://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/tree/drivers/input/mouse/synaptics.c>`_�h]�h9�the kernel source�����}�(hhhjr  ubah}�(h]�h]�h]�h]�h]��name��the kernel source�h��chttp://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/tree/drivers/input/mouse/synaptics.c�uhh{hji  ubh)��}�(h�f <http://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/tree/drivers/input/mouse/synaptics.c>�h]�h}�(h]��the-kernel-source�ah]�h]��the kernel source�ah]�h]��refuri�j�  uhhh�Khji  ubh9�)
(search for “topbuttonpad_pnp_ids”).�����}�(h�%
(search for "topbuttonpad_pnp_ids").�hji  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhEhh,hK~hj�  hhubeh}�(h]�(�kernel-support�j�  eh]�h]�(�kernel support��t440_support_identification�eh]�h]�uhh-hh/hhhh,hKqjY  }�j�  j�  sj[  }�j�  j�  subeh}�(h]�(�!lenovo-40-series-touchpad-support�h+eh]�h]�(�"lenovo *40 series touchpad support��t440_support�eh]�h]�uhh-hhhhhh,hKjY  }�j�  h sj[  }�h+h subeh}�(h]�h]�h]�h]�h]��source�h,uhh�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(h2N�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}�(�git_version�h �substitution_definition���)��}�(h�,.. |git_version| replace:: :commit:`8610c18`�h]�h|)��}�(h�git commit 8610c18�h]�h9�git commit 8610c18�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]��internal���refuri��?https://gitlab.freedesktop.org/libinput/libinput/commit/8610c18�uhh{hj  ubah}�(h]�h]�h]�j  ah]�h]�uhj  h�<rst_prolog>�hKhhub�git_version_full�j  )��}�(h�].. |git_version_full| replace:: :commit:`<function get_git_version_full at 0x7fe160dcce18>`

�h]�h|)��}�(h�<git commit <function get_git_version_full at 0x7fe160dcce18>�h]�h9�<git commit <function get_git_version_full at 0x7fe160dcce18>�����}�(hhhj/  ubah}�(h]�h]�h]�h]�h]��internal���refuri��ihttps://gitlab.freedesktop.org/libinput/libinput/commit/<function get_git_version_full at 0x7fe160dcce18>�uhh{hj+  ubah}�(h]�h]�h]�j*  ah]�h]�uhj  hj)  hKhhubu�substitution_names�}�(�git_version�j  �git_version_full�j*  u�refnames�}��refids�}�(h+]�h ah`]�hVajO  ]�jE  aj�  ]�j�  aj�  ]�j�  au�nameids�}�(j�  h+j�  j�  jV  h`jU  jR  h�h�j6  j3  j�  jO  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j(  j%  jZ  jW  j�  j�  u�	nametypes�}�(j�  �j�  NjV  �jU  Nh��j6  �j�  �j�  Nj�  �j�  Nj�  �j�  Nj(  �jZ  �j�  �uh}�(h+h/j�  h/h`hajR  hah�h�j3  j-  jO  j]  j�  j]  j�  j�  j�  j�  j�  j�  j�  j�  j%  j  jW  jQ  j�  j�  h�h�u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]��transform_messages�]�(h �system_message���)��}�(hhh]�hF)��}�(hhh]�h9�2Hyperlink target "t440-support" is not referenced.�����}�(hhhjt  ubah}�(h]�h]�h]�h]�h]�uhhEhjq  ubah}�(h]�h]�h]�h]�h]��level�K�type��INFO��source�h,�line�Kuhjo  ubjp  )��}�(hhh]�hF)��}�(hhh]�h9�;Hyperlink target "t440-support-overview" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhEhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h,�line�Kuhjo  ubjp  )��}�(hhh]�hF)��}�(hhh]�h9�;Hyperlink target "t440-support-btn-size" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhEhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h,�line�K)uhjo  ubjp  )��}�(hhh]�hF)��}�(hhh]�h9�?Hyperlink target "t440-support-btn-behavior" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhEhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h,�line�K9uhjo  ubjp  )��}�(hhh]�hF)��}�(hhh]�h9�AHyperlink target "t440-support-identification" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhEhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h,�line�Kruhjo  ube�transformer�N�
decoration�Nhhub.