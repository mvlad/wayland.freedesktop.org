��5+      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �block_quote���)��}�(hhh]�(h �substitution_definition���)��}�(h�,.. |git_version| replace:: :commit:`e2e8db1`�h]�h �	reference���)��}�(h�git commit e2e8db1�h]�h �Text����git commit e2e8db1�����}�(hh�parent�huba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]��internal���refuri��?https://gitlab.freedesktop.org/libinput/libinput/commit/e2e8db1�u�tagname�hh hubah!}�(h#]�h%]�h']��git_version�ah)]�h+]�uh0h�source��<rst_prolog>��line�Kh hubh)��}�(h�].. |git_version_full| replace:: :commit:`<function get_git_version_full at 0x7f342f19c2f0>`

�h]�h)��}�(h�<git commit <function get_git_version_full at 0x7f342f19c2f0>�h]�h�<git commit <function get_git_version_full at 0x7f342f19c2f0>�����}�(hhh h?ubah!}�(h#]�h%]�h']�h)]�h+]��internal���refuri��ihttps://gitlab.freedesktop.org/libinput/libinput/commit/<function get_git_version_full at 0x7f342f19c2f0>�uh0hh h;ubah!}�(h#]�h%]�h']��git_version_full�ah)]�h+]�uh0hh8h9h:Kh hubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h	h hhhh8Nh:Nubh �target���)��}�(h�.. _touchpad_jumping_cursor:�h]�h!}�(h#]�h%]�h']�h)]�h+]��refid��touchpad-jumping-cursor�uh0h]h:Kh hhhh8�D/home/whot/code/libinput/build/doc/user/touchpad-jumping-cursors.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�Touchpad jumping cursor bugs�h]�h�Touchpad jumping cursor bugs�����}�(hhuh hshhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh hnhhh8hkh:Kubh �	paragraph���)��}�(hXy  A common bug encountered on touchpads is a cursor jump when alternating
between fingers on a multi-touch-capable touchpad. For example, after moving
the cursor a user may use a second finger in the software button area to
physically click the touchpad. Upon setting the finger down, the cursor
exhibits a jump towards the bottom left or right, depending on the finger
position.�h]�hXy  A common bug encountered on touchpads is a cursor jump when alternating
between fingers on a multi-touch-capable touchpad. For example, after moving
the cursor a user may use a second finger in the software button area to
physically click the touchpad. Upon setting the finger down, the cursor
exhibits a jump towards the bottom left or right, depending on the finger
position.�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh�)��}�(h��When libinput detects a cursor jump it prints a bug warning to the log with
the text **"Touch jump detected and discarded."** and a link to this page.�h]�(h�UWhen libinput detects a cursor jump it prints a bug warning to the log with
the text �����}�(h�UWhen libinput detects a cursor jump it prints a bug warning to the log with
the text �h h�hhh8Nh:Nubh �strong���)��}�(h�(**"Touch jump detected and discarded."**�h]�h�(“Touch jump detected and discarded.”�����}�(hhh h�ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h h�ubh� and a link to this page.�����}�(h� and a link to this page.�h h�hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh�)��}�(h��In most cases, this is a bug in the kernel driver and to libinput it appears
that the touch point moves from its previous position. The pointer jump can
usually be seen in the evemu-record output for the device:�h]�h��In most cases, this is a bug in the kernel driver and to libinput it appears
that the touch point moves from its previous position. The pointer jump can
usually be seen in the evemu-record output for the device:�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh �literal_block���)��}�(hXw  E: 249.206319 0000 0000 0000    # ------------ SYN_REPORT (0) ----------
E: 249.218008 0003 0035 3764    # EV_ABS / ABS_MT_POSITION_X    3764
E: 249.218008 0003 0036 2221    # EV_ABS / ABS_MT_POSITION_Y    2221
E: 249.218008 0003 003a 0065    # EV_ABS / ABS_MT_PRESSURE      65
E: 249.218008 0003 0000 3764    # EV_ABS / ABS_X                3764
E: 249.218008 0003 0001 2216    # EV_ABS / ABS_Y                2216
E: 249.218008 0003 0018 0065    # EV_ABS / ABS_PRESSURE         65
E: 249.218008 0000 0000 0000    # ------------ SYN_REPORT (0) ----------
E: 249.230881 0003 0035 3752    # EV_ABS / ABS_MT_POSITION_X    3752
E: 249.230881 0003 003a 0046    # EV_ABS / ABS_MT_PRESSURE      46
E: 249.230881 0003 0000 3758    # EV_ABS / ABS_X                3758
E: 249.230881 0003 0018 0046    # EV_ABS / ABS_PRESSURE         46
E: 249.230881 0000 0000 0000    # ------------ SYN_REPORT (0) ----------
E: 249.242648 0003 0035 1640    # EV_ABS / ABS_MT_POSITION_X    1640
E: 249.242648 0003 0036 4681    # EV_ABS / ABS_MT_POSITION_Y    4681
E: 249.242648 0003 003a 0025    # EV_ABS / ABS_MT_PRESSURE      25
E: 249.242648 0003 0000 1640    # EV_ABS / ABS_X                1640
E: 249.242648 0003 0001 4681    # EV_ABS / ABS_Y                4681
E: 249.242648 0003 0018 0025    # EV_ABS / ABS_PRESSURE         25
E: 249.242648 0000 0000 0000    # ------------ SYN_REPORT (0) ----------
E: 249.254568 0003 0035 1648    # EV_ABS / ABS_MT_POSITION_X    1648
E: 249.254568 0003 003a 0027    # EV_ABS / ABS_MT_PRESSURE      27
E: 249.254568 0003 0000 1644    # EV_ABS / ABS_X                1644
E: 249.254568 0003 0018 0027    # EV_ABS / ABS_PRESSURE         27�h]�hXw  E: 249.206319 0000 0000 0000    # ------------ SYN_REPORT (0) ----------
E: 249.218008 0003 0035 3764    # EV_ABS / ABS_MT_POSITION_X    3764
E: 249.218008 0003 0036 2221    # EV_ABS / ABS_MT_POSITION_Y    2221
E: 249.218008 0003 003a 0065    # EV_ABS / ABS_MT_PRESSURE      65
E: 249.218008 0003 0000 3764    # EV_ABS / ABS_X                3764
E: 249.218008 0003 0001 2216    # EV_ABS / ABS_Y                2216
E: 249.218008 0003 0018 0065    # EV_ABS / ABS_PRESSURE         65
E: 249.218008 0000 0000 0000    # ------------ SYN_REPORT (0) ----------
E: 249.230881 0003 0035 3752    # EV_ABS / ABS_MT_POSITION_X    3752
E: 249.230881 0003 003a 0046    # EV_ABS / ABS_MT_PRESSURE      46
E: 249.230881 0003 0000 3758    # EV_ABS / ABS_X                3758
E: 249.230881 0003 0018 0046    # EV_ABS / ABS_PRESSURE         46
E: 249.230881 0000 0000 0000    # ------------ SYN_REPORT (0) ----------
E: 249.242648 0003 0035 1640    # EV_ABS / ABS_MT_POSITION_X    1640
E: 249.242648 0003 0036 4681    # EV_ABS / ABS_MT_POSITION_Y    4681
E: 249.242648 0003 003a 0025    # EV_ABS / ABS_MT_PRESSURE      25
E: 249.242648 0003 0000 1640    # EV_ABS / ABS_X                1640
E: 249.242648 0003 0001 4681    # EV_ABS / ABS_Y                4681
E: 249.242648 0003 0018 0025    # EV_ABS / ABS_PRESSURE         25
E: 249.242648 0000 0000 0000    # ------------ SYN_REPORT (0) ----------
E: 249.254568 0003 0035 1648    # EV_ABS / ABS_MT_POSITION_X    1648
E: 249.254568 0003 003a 0027    # EV_ABS / ABS_MT_PRESSURE      27
E: 249.254568 0003 0000 1644    # EV_ABS / ABS_X                1644
E: 249.254568 0003 0018 0027    # EV_ABS / ABS_PRESSURE         27�����}�(hhh h�ubah!}�(h#]�h%]�h']�h)]�h+]��	xml:space��preserve�uh0h�h:Kh hnhhh8hkubh�)��}�(hXM  In this recording, the pointer jumps from its position 3752/2216 to
1640/4681 within a single frame. On this particular touchpad, this would
represent a physical move of almost 50mm. libinput detects some of these
jumps and discards the movement but otherwise continues as usual. However,
the bug should be fixed at the kernel level.�h]�hXM  In this recording, the pointer jumps from its position 3752/2216 to
1640/4681 within a single frame. On this particular touchpad, this would
represent a physical move of almost 50mm. libinput detects some of these
jumps and discards the movement but otherwise continues as usual. However,
the bug should be fixed at the kernel level.�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K2h hnhhubh�)��}�(h��When you encounter the warning in the log, please generate an evemu
recording of your touchpad and file a bug. See :ref:`reporting_bugs` for more
details.�h]�(h�sWhen you encounter the warning in the log, please generate an evemu
recording of your touchpad and file a bug. See �����}�(h�sWhen you encounter the warning in the log, please generate an evemu
recording of your touchpad and file a bug. See �h h�hhh8Nh:Nub�sphinx.addnodes��pending_xref���)��}�(h�:ref:`reporting_bugs`�h]�h �inline���)��}�(hh�h]�h�reporting_bugs�����}�(hhh h�ubah!}�(h#]�h%]�(�xref��std��std-ref�eh']�h)]�h+]�uh0h�h h�ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�j   �refexplicit���	reftarget��reporting_bugs��refdoc��touchpad-jumping-cursors��refwarn��uh0h�h8hkh:K8h h�ubh� for more
details.�����}�(h� for more
details.�h h�hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K8h hnhhubeh!}�(h#]�(�touchpad-jumping-cursor-bugs�hjeh%]�h']�(�touchpad jumping cursor bugs��touchpad_jumping_cursor�eh)]�h+]�uh0hlh hhhh8hkh:K�expect_referenced_by_name�}�j%  h_s�expect_referenced_by_id�}�hjh_subeh!}�(h#]�h%]�h']�h)]�h+]��source�hkuh0h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hqN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�jO  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�hk�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�N�gettext_compact��ub�reporter�N�indirect_targets�]��substitution_defs�}�(h5hhTh;u�substitution_names�}�(�git_version�h5�git_version_full�hTu�refnames�}��refids�}�hj]�h_as�nameids�}�(j%  hjj$  j!  u�	nametypes�}�(j%  �j$  Nuh#}�(hjhnj!  hnu�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]��transform_messages�]�h �system_message���)��}�(hhh]�h�)��}�(hhh]�h�=Hyperlink target "touchpad-jumping-cursor" is not referenced.�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type��INFO��source�hk�line�Kuh0j�  uba�transformer�N�
decoration�Nhhub.